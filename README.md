# Readme #

## Change logs ##

### Version 2.0 ###
* Fully Redesign
* Booking Online

### Version 1.01 ###
* Fix lỗi do chị xuyến nêu ra,
* Function CreateBookingRequest from Mr Chiến
* Update title và meta description
* Enable cache

     1. Setting tại application/config/setting.php
     2. Mặc định cache 15 phút
     3. Fix lỗi nếu lần kiểm tra chiều đi rỗng sẽ ko cache
     4. Cache này thực hiện trên tất cả các khách chứ ko phải theo session
     5. Chưa có cơ chế xóa cache

* Thêm method append_js cho phép di chuyển js vào folder module, ko dính đến theme nữa
* Basic redesign
* Design lại giao diện chọn vé