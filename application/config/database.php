<?php
defined('BASEPATH') OR exit('No direct script access allowed');

switch (DBENV) {
    case 'PROD':
        $active_group = 'prod';
        break;
    case 'BETA':
        $active_group = 'beta';
        break;
    case 'UAT':
        $active_group = 'uat';
        break;
    case 'DEV':
        $active_group = 'dev';
        break;
    default:
        exit('Missing Database Configuration');
        break;
}


$query_builder = TRUE;


$db['prod'] = array(
    'dsn' => '',
    'hostname' => 'localhost',
    'username' => 'gotadi_z',
    'password' => '',
    'database' => 'gotadi_z',
    'dbdriver' => 'mysqli',
    'dbprefix' => '',
    'pconnect' => FALSE,
    'db_debug' => (ENVIRONMENT !== 'production'),
    'cache_on' => FALSE,
    'cachedir' => '',
    'char_set' => 'utf8',
    'dbcollat' => 'utf8_general_ci',
    'swap_pre' => '',
    'encrypt' => FALSE,
    'compress' => FALSE,
    'stricton' => FALSE,
    'failover' => array(),
    'save_queries' => TRUE
);

$db['beta'] = array(
    'dsn' => '',
    'hostname' => 'localhost',
    'username' => 'gotadi_dev',
    'password' => '9V!qTVvnsh5i',
    'database' => 'gotadi_dev',
    'dbdriver' => 'mysqli',
    'dbprefix' => '',
    'pconnect' => FALSE,
    'db_debug' => (ENVIRONMENT !== 'production'),
    'cache_on' => FALSE,
    'cachedir' => '',
    'char_set' => 'utf8',
    'dbcollat' => 'utf8_general_ci',
    'swap_pre' => '',
    'encrypt' => FALSE,
    'compress' => FALSE,
    'stricton' => FALSE,
    'failover' => array(),
    'save_queries' => TRUE
);

$db['uat'] = array(
    'dsn' => '',
    'hostname' => 'localhost',
    'username' => 'uat_m_gotadi',
    'password' => 'uat_m_gotadi',
    'database' => 'uat_m_gotadi',
    'dbdriver' => 'mysqli',
    'dbprefix' => '',
    'pconnect' => FALSE,
    'db_debug' => (ENVIRONMENT !== 'production'),
    'cache_on' => FALSE,
    'cachedir' => '',
    'char_set' => 'utf8',
    'dbcollat' => 'utf8_general_ci',
    'swap_pre' => '',
    'encrypt' => FALSE,
    'compress' => FALSE,
    'stricton' => FALSE,
    'failover' => array(),
    'save_queries' => TRUE
);


$db['dev'] = array(
    'dsn' => '',
    'hostname' => 'localhost',
    'username' => 'root',
    'password' => '',
    'database' => 'gotadi_m',
    'dbdriver' => 'mysqli',
    'dbprefix' => '',
    'pconnect' => FALSE,
    'db_debug' => (ENVIRONMENT !== 'production'),
    'cache_on' => FALSE,
    'cachedir' => '',
    'char_set' => 'utf8',
    'dbcollat' => 'utf8_general_ci',
    'swap_pre' => '',
    'encrypt' => FALSE,
    'compress' => FALSE,
    'stricton' => FALSE,
    'failover' => array(),
    'save_queries' => TRUE
);
