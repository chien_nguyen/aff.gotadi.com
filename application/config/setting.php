<?php

$config['version'] = '2.0'; //Release 27/10/2015

$config['backend_theme'] = 'admin';

(ENVIRONMENT == 'production') ?
                $config['desktop_link'] = 'http://www.gotadi.com/?ref=mobile' :
                $config['desktop_link'] = 'http://uat-www.gotadi.com:8086/?ref=mobile';

$config ['cache_enable'] = false;

$config['cache_ttl'] = 750;

/* * *************************SMARTLINK CONFIGURATION**************************** */
switch (SMLENV) {
    case 'PROD':
        $config['smartlink_gateway'] = 'https://payment.smartlink.com.vn/vpcpay.do';
        $config['smartlink_access_code'] = 'H1G2T3R4A5V6E7L8';
        $config['smartlink_merchant'] = 'HGTRAVEL';
        break;
    case 'BETA':
        $config['smartlink_gateway'] = 'https://payment.smartlink.com.vn/vpcpay.do';
        $config['smartlink_access_code'] = 'H1G2T3R4A5V6E7L8';
        $config['smartlink_merchant'] = 'HGTRAVEL';
        break;
    case 'UAT':
        $config['smartlink_gateway'] = 'https://payment.smartlink.com.vn/vpcpay.do';
        $config['smartlink_access_code'] = 'H1G2T3R4A5V6E7L8';
        $config['smartlink_merchant'] = 'HGTRAVEL';
        break;
    case 'DEV':
        $config['smartlink_gateway'] = 'http://payment.smartlink.com.vn/gateway/vpcpay.do';
        $config['smartlink_access_code'] = 'ECAFAB';
        $config['smartlink_merchant'] = 'SMLTEST';
        break;
    default :
        header('HTTP/1.1 503 Service Unavailable.', TRUE, 503);
        echo 'The application environment is not set correctly.';
        exit(1); // EXIT_ERROR
}
//payment fee
$config['atmFee'] = 1.2 / 100;

//RESPONSE MESSAGE
$config['smartlink_response_message'] = array(
    'Giao dich thanh cong',
    'Ngan hang tu choi thanh toan: the/tai khoan bi khoa',
    'Loi so 2',
    'The het han',
    'Qua so lan giao dich cho phep. (Sai OTP, qua han muc trong ngay)',
    'Khong co tra loi tu Ngan hang',
    'Loi giao tiep voi Ngan hang',
    'Tai khoan khong du tien',
    'Loi du lieu truyen',
    'Kieu giao dich khong duoc ho tro',
    'Loi khong xac dinh',
);


/* * *****************INTERNATIONAL CARD CONFIGURATION************************* */
switch (CARDENV) {
    case 'PROD':
        $config['visaGateway'] = 'https://migs.mastercard.com.au/vpcpay';
        $config['visaAccessCode'] = '72AD46B6';
        $config['visaMerchTxnRef'] = 'TEST';
        $config['visaMerchant'] = 'test03051980';
        break;
    case 'BETA':
        $config['visaGateway'] = 'https://migs.mastercard.com.au/vpcpay';
        $config['visaAccessCode'] = '72AD46B6';
        $config['visaMerchTxnRef'] = 'TEST';
        $config['visaMerchant'] = 'test03051980';
        break;
    case 'UAT':
        $config['visaGateway'] = 'https://migs.mastercard.com.au/vpcpay';
        $config['visaAccessCode'] = '72AD46B6';
        $config['visaMerchTxnRef'] = 'TEST';
        $config['visaMerchant'] = 'test03051980';
        break;
    case 'DEV':
        $config['visaGateway'] = 'https://migs.mastercard.com.au/vpcpay';
        $config['visaAccessCode'] = '72AD46B6';
        $config['visaMerchTxnRef'] = 'TEST';
        $config['visaMerchant'] = 'test03051980';
        break;
    default :
        header('HTTP/1.1 503 Service Unavailable.', TRUE, 503);
        echo 'The application environment is not set correctly.';
        exit(1); // EXIT_ERROR
}

//payment fee
$config['visaFee'] = 2.65 / 100;


//Booking services
(BOOKENV == 'PROD') ?
                $config['bookingURL'] = 'http://wcf.gotadi.com/BookingServices.svc?wsdl' :
                $config['bookingURL'] = 'http://uat-wcf.gotadi.com:8086/BookingServices.svc?wsdl';


/* * ******************************API****************************************** */

//JETSTAR
switch (JSENV) {
    case 'PROD':
        $config['jetstarAPI'] = 'http://uat-api-m.gotadi.com:8086/production/';
        break;
    case 'BETA':
        $config['jetstarAPI'] = 'http://uat-api-m.gotadi.com:8086/production/';
        break;
    case 'UAT':
        $config['jetstarAPI'] = 'http://uat-api-m.gotadi.com:8086/preprod/';
        break;
    case 'DEV':
        $config['jetstarAPI'] = 'http://uat-api-m.gotadi.com:8086/preprod/';
        break;
    default:
        break;
}
//VIETJET
switch (VJENV) {
    case 'PROD':
        $config['vietjetAPI'] = 'http://uat-api-m.gotadi.com:8086/production/';
        break;
    case 'BETA':
        $config['vietjetAPI'] = 'http://uat-api-m.gotadi.com:8086/production/';
        break;
    case 'UAT':
        $config['vietjetAPI'] = 'http://uat-api-m.gotadi.com:8086/preprod/';
        break;
    case 'DEV':
        $config['vietjetAPI'] = 'http://uat-api-m.gotadi.com:8086/preprod/';
        break;
    default:
        break;
}
//VNA
switch (VNENV) {
    case 'PROD':
        $config['vnaAPI'] = 'http://uat-api-m.gotadi.com:8086/production/';
        break;
    case 'BETA':
        $config['vnaAPI'] = 'http://uat-api-m.gotadi.com:8086/production/';
        break;
    case 'UAT':
        $config['vnaAPI'] = 'http://uat-api-m.gotadi.com:8086/preprod/';
        break;
    case 'DEV':
        $config['vnaAPI'] = 'http://uat-api-m.gotadi.com:8086/preprod/';
        break;
    default:
        break;
}
