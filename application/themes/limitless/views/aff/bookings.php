<!-- Main content -->
<div class="content-wrapper">
    <!-- Theme JS files -->
    <script type="text/javascript" src="<?php echo $this->template->dir(); ?>assets/js/core/app.js"></script>
    <!-- /theme JS files -->
    <!-- Basic table -->
    <div class="panel panel-flat">
        <div class="panel-heading">
            <h5 class="panel-title">Booking hôm nay</h5>
            <div class="heading-elements">
                <ul class="icons-list">
                    <li><a data-action="collapse"></a></li>
                    <li><a data-action="reload"></a></li>
                    <li><a data-action="close"></a></li>
                </ul>
            </div>
        </div>




        <div class="table-responsive">
            <table class="table">
                <thead>
                    <tr>
                        <th>STT</th>
                        <th>Tên khách hàng</th>
                        <th>Ngày book</th>
                        <th>Hãng</th>
                        <th>Loại vé</th>
                        <th>Số tiền</th>
                        <th>Trạng thái</th>
                        <th>Conmission</th>
                    </tr>
                </thead>
                <?php if($bookings != false) :?>
                <tbody>


                    <?php
                    $flagBookings = true; //Chỉ có 1 booking
                    foreach ($bookings['BookingDO'] as $value) {
                        if (is_array($value)) {
                            $flagBookings = false;
                        }
                    }
                    ?>
                    <?php $booking = $bookings['BookingDO']; ?>
                    <?php if ($flagBookings == true): ?>
                        <tr>
                            <td>1</td>
                            <td><?php echo $booking['LastName'] . $booking['FirstName']; ?></td>
                            <td><?php echo $booking['BookingDate']; ?></td>
                            <td><?php echo $booking['Provider']; ?></td>
                            <td><?php echo $booking['BusinessDetailInfo']; ?></td>
                            <td><?php echo number_format($booking['PaymentAmount']); ?></td>
                            <td><?php echo $booking['BookingStatus']; ?></td>
                            <td>10.000</td>
                        </tr>
                    <?php else: ?>
                        <?php foreach ($bookings['BookingDO'] as $booking) : ?>
                            <tr>
                                <td>1</td>
                                <td><?php echo $booking['LastName'] . $booking['FirstName']; ?></td>
                                <td><?php echo $booking['BookingDate']; ?></td>
                                <td><?php echo $booking['Provider']; ?></td>
                                <td><?php echo $booking['BusinessDetailInfo']; ?></td>
                                <td><?php echo number_format($booking['PaymentAmount']); ?></td>
                                <td><?php echo $booking['BookingStatus']; ?></td>
                                <td>10.000</td>
                            </tr>

                        <?php endforeach; ?>
                    <?php endif; ?>
                </tbody>
                <?php endif;?>
            </table>
        </div>
    </div>
    <!-- /basic table -->

    <!-- Striped rows -->
    <div class="panel panel-flat">
        <div class="panel-heading">
            <h5 class="panel-title">Booking trong tháng</h5>
            <div class="heading-elements">
                <ul class="icons-list">
                    <li><a data-action="collapse"></a></li>
                    <li><a data-action="reload"></a></li>
                    <li><a data-action="close"></a></li>
                </ul>
            </div>
        </div>


        <div class="table-responsive">
            <table class="table">
                <thead>
                    <tr>
                        <th>STT</th>
                        <th>Tên khách hàng</th>
                        <th>Ngày book</th>
                        <th>Hãng</th>
                        <th>Loại vé</th>
                        <th>Số tiền</th>
                        <th>Trạng thái</th>
                        <th>Conmission</th>
                    </tr>
                </thead>
                <?php if($month_books != false) :?>
                <tbody>
                    
                    <?php
                    $flagBookings2 = true; //Chỉ có 1 booking
                    foreach ($month_books['BookingDO'] as $value) {
                        if (is_array($value)) {
                            $flagBookings2 = false;
                        }
                    }
                    ?>
                    <?php $month_book = $month_books['BookingDO']; ?>
                    <?php if ($flagBookings2 == true): ?>

                   <tr>
                            <td>1</td>
                            <td><?php echo $month_books['LastName'] . $month_books['FirstName']; ?></td>
                            <td><?php echo $month_books['BookingDate']; ?></td>
                            <td><?php echo $month_books['Provider']; ?></td>
                            <td><?php echo $month_books['BusinessDetailInfo']; ?></td>
                            <td><?php echo number_format($month_books['PaymentAmount']); ?></td>
                            <td><?php echo $month_books['BookingStatus']; ?></td>
                            <td>10.000</td>
                        </tr>
                    <?php else: ?>
                        <?php foreach ($month_books['BookingDO'] as $items) : ?>
                            <tr>
                                <td>1</td>
                                <td><?php echo $items['LastName'] . $items['FirstName']; ?></td>
                                <td><?php echo $items['BookingDate']; ?></td>
                                <td><?php echo $items['Provider']; ?></td>
                                <td><?php echo $items['BusinessDetailInfo']; ?></td>
                                <td><?php echo number_format($items['PaymentAmount']); ?></td>
                                <td><?php echo $items['BookingStatus']; ?></td>
                                <td>10.000</td>
                            </tr>
                     <?php endforeach; ?>
                    <?php endif; ?>
                </tbody>
                <?php endif;?>
            </table>
        </div>
    </div>
    <!-- /striped rows -->



</div>
<!-- /main content -->
