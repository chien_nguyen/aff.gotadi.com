
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Limitless - Responsive Web Application Kit by Eugene Kopyov</title>

        <!-- Global stylesheets -->
        <link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
        <link href="<?php echo $this->template->dir() ?>assets/css/icons/icomoon/styles.css" rel="stylesheet" type="text/css">
        <link href="<?php echo $this->template->dir() ?>assets/css/bootstrap.min.css" rel="stylesheet" type="text/css">
        <link href="<?php echo $this->template->dir() ?>assets/css/core.min.css" rel="stylesheet" type="text/css">
        <link href="<?php echo $this->template->dir() ?>assets/css/components.min.css" rel="stylesheet" type="text/css">
        <link href="<?php echo $this->template->dir() ?>assets/css/colors.min.css" rel="stylesheet" type="text/css">
        <!-- /global stylesheets -->

        <!-- Core JS files -->
        <script type="text/javascript" src="<?php echo $this->template->dir() ?>assets/js/plugins/loaders/pace.min.js"></script>
        <script type="text/javascript" src="<?php echo $this->template->dir() ?>assets/js/core/libraries/jquery.min.js"></script>
        <script type="text/javascript" src="<?php echo $this->template->dir() ?>assets/js/core/libraries/bootstrap.min.js"></script>
        <script type="text/javascript" src="<?php echo $this->template->dir() ?>assets/js/plugins/loaders/blockui.min.js"></script>
        <script type="text/javascript" src="<?php echo $this->template->dir() ?>assets/js/plugins/ui/nicescroll.min.js"></script>
        <script type="text/javascript" src="<?php echo $this->template->dir() ?>assets/js/plugins/ui/drilldown.js"></script>
        <!-- /core JS files -->

        <!-- Theme JS files -->
        <script type="text/javascript" src="<?php echo $this->template->dir() ?>assets/js/plugins/forms/styling/uniform.min.js"></script>

        <script type="text/javascript" src="<?php echo $this->template->dir() ?>assets/js/core/app.js"></script>
        <script type="text/javascript" src="<?php echo $this->template->dir() ?>assets/js/pages/login.js"></script>
        <!-- /theme JS files -->

    </head>

    <body class="bg-slate-800">

        <!-- Page container -->
        <div class="page-container login-container">

            <!-- Page content -->
            <div class="page-content">

                <!-- Main content -->
                <div class="content-wrapper">

                    <!-- Registration form -->
                    <?php echo form_open()?>
                        <div class="row">
                            <div class="col-lg-6 col-lg-offset-3">
                                <div class="panel registration-form">
                                    <div class="panel-body">
                                        <div class="text-center">
                                            <div class="icon-object border-success text-success"><i class="icon-plus3"></i></div>
                                            <h5 class="content-group-lg">Tạo một tài khoản <small class="display-block">Yêu cầu nhập đủ các trường dưới đây</small></h5>
                                        </div>

                                        <div class="form-group has-feedback">
                                            <?php echo form_input('username', $username, 'class="form-control" placeholder="Tên đăng nhập"')?>
                                            <div class="form-control-feedback">
                                                <i class="icon-user-plus text-muted"></i>
                                            </div>
                                            <?php echo form_error('username')?>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group has-feedback">
                                                    <?php echo form_input('firstName', $firstName, 'class="form-control" placeholder="Họ"')?>
                                                    <div class="form-control-feedback">
                                                        <i class="icon-user-check text-muted"></i>
                                                    </div>
                                                    <?php echo form_error('firstName')?>
                                                </div>
                                            </div>

                                            <div class="col-md-6">
                                                <div class="form-group has-feedback">
                                                    <?php echo form_input('lastName', $lastName, 'class="form-control" placeholder="Tên và tên đệm"')?>
                                                    <div class="form-control-feedback">
                                                        <i class="icon-user-check text-muted"></i>
                                                    </div>
                                                    <?php echo form_error('lastName')?>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group has-feedback">
                                                    <?php echo form_password('password', $password, 'class="form-control" placeholder="Mật khẩu"')?>
                                                    <div class="form-control-feedback">
                                                        <i class="icon-user-lock text-muted"></i>
                                                    </div>
                                                    <?php echo form_error('password')?>
                                                </div>
                                            </div>

                                            <div class="col-md-6">
                                                <div class="form-group has-feedback">
                                                    <?php echo form_password('passwordConfirm', $passwordConfirm, 'class="form-control" placeholder="Xác nhận mật khẩu"')?>
                                                    <div class="form-control-feedback">
                                                        <i class="icon-user-lock text-muted"></i>
                                                    </div>
                                                    <?php echo form_error('passwordConfirm')?>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group has-feedback">
                                                    <?php echo form_input('email', $email, 'class="form-control" placeholder="Địa chỉ email"')?>
                                                    <div class="form-control-feedback">
                                                        <i class="icon-mention text-muted"></i>
                                                    </div>
                                                     <?php echo form_error('email')?>
                                                </div>
                                            </div>

                                            <div class="col-md-6">
                                                <div class="form-group has-feedback">
                                                    <?php echo form_input('emailConfirm', $emailConfirm, 'class="form-control" placeholder="Xác nhận email"')?>
                                                    <div class="form-control-feedback">
                                                        <i class="icon-mention text-muted"></i>
                                                    </div>
                                                     <?php echo form_error('emailConfirm')?>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group">

                                            <div class="checkbox">
                                                <label>
                                                    <input type="checkbox" class="styled" name="subcribe" checked="checked">
                                                    Nhận email hàng tháng về chương trình
                                                </label>
                                            </div>

                                            <div class="checkbox">
                                                <label>
                                                    <?php echo form_checkbox('acceptTerm', $acceptTerm, $acceptTerm = 1, 'class="styled')?>
                                                    Tôi đồng ý với các <a href="#">điều khoản dịch vụ</a>
                                                </label>
                                            </div>
                                             <?php echo form_error('acceptTerm')?>
                                        </div>

                                        <div class="text-right">
                                            <a href="<?php echo base_url('aff/login')?>" class="btn btn-link"><i class="icon-arrow-left13 position-left"></i> Trở lại form đăng nhập</a>
                                            <button type="submit" class="btn bg-teal-400 btn-labeled btn-labeled-right ml-10"><b><i class="icon-plus3"></i></b> Đăng ký</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php echo form_close()?>
                    <!-- /registration form -->

                </div>
                <!-- /main content -->

            </div>
            <!-- /page content -->


            <!-- Footer -->
            <div class="footer text-muted">
                &copy; 2015. <a href="#">Gotdi Limitless</a> by <a href="#" target="_blank">HG Travel</a>
            </div>
            <!-- /footer -->

        </div>
        <!-- /page container -->

    </body>
</html>
