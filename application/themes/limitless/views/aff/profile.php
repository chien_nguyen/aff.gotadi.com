<div class="content-wrapper">

    <!-- Toolbar -->
    <div class="navbar navbar-default navbar-component navbar-xs">
        <ul class="nav navbar-nav visible-xs-block">
            <li class="full-width text-center"><a data-toggle="collapse" data-target="#navbar-filter"><i class="icon-menu7"></i></a></li>
        </ul>

        <div class="navbar-collapse collapse" id="navbar-filter">
            <ul class="nav navbar-nav element-active-slate-400">
                <li><a href="#activity" data-toggle="tab"><i class="icon-menu7 position-left"></i> Activity</a></li>
                <li class="active"><a href="#settings" data-toggle="tab"><i class="icon-cog3 position-left"></i> Settings</a></li>
            </ul>
        </div>
    </div>
    <!-- /toolbar -->


    <!-- User profile -->
    <div class="row">
        <div class="col-lg-9">
            <div class="tabbable">
                <div class="tab-content">
                    <div class="tab-pane fade" id="activity">

                        <!-- Timeline -->
                        <div class="timeline timeline-left content-group">
                            <div class="timeline-container">
                                <!-- Date stamp -->
                                <div class="timeline-date text-muted">
                                    <i class="icon-history position-left"></i> <span class="text-semibold">Monday</span>, April 18
                                </div>
                                <!-- /date stamp -->


                                <!-- Invoices -->
                                <div class="timeline-row">
                                    <div class="timeline-icon">
                                        <div class="bg-warning-400">
                                            <i class="icon-cash3"></i>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-lg-6">
                                            <div class="panel border-left-lg border-left-danger invoice-grid timeline-content">
                                                <div class="panel-body">
                                                    <div class="row">
                                                        <div class="col-sm-6">
                                                            <h6 class="text-semibold no-margin-top"><?php echo $user['firstName'] . ' ' . $user['lastName'] ?></h6>
                                                            <ul class="list list-unstyled">
                                                                <li>Email: <?php echo $user['email']; ?></li>
                                                                <li>Số điện thoại: <?php echo $user['phone']; ?></li>
                                                                <li>Địa chỉ: <?php echo $user['ward'] . ' , ' . $user['district'] . ' , ' . $user['city']; ?></li>
                                                            </ul>
                                                        </div>

                                                        <div class="col-sm-6">
                                                            <h6 class="text-semibold text-right no-margin-top">$8,750</h6>
                                                            <ul class="list list-unstyled text-right">
                                                                <li>Method: <span class="text-semibold">SWIFT</span></li>
                                                                <li class="dropdown">
                                                                    Status: &nbsp;
                                                                    <a href="#" class="label bg-danger-400 dropdown-toggle" data-toggle="dropdown">Overdue <span class="caret"></span></a>
                                                                    <ul class="dropdown-menu dropdown-menu-right">
                                                                        <li class="active"><a href="#"><i class="icon-alert"></i> Overdue</a></li>
                                                                        <li><a href="#"><i class="icon-alarm"></i> Pending</a></li>
                                                                        <li><a href="#"><i class="icon-checkmark3"></i> Paid</a></li>
                                                                        <li class="divider"></li>
                                                                        <li><a href="#"><i class="icon-spinner2 spinner"></i> On hold</a></li>
                                                                        <li><a href="#"><i class="icon-cross2"></i> Canceled</a></li>
                                                                    </ul>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="panel-footer">
                                                    <ul>
                                                        <li><span class="status-mark border-danger position-left"></span> Due: <span class="text-semibold">2015/02/25</span></li>
                                                    </ul>

                                                    <ul class="pull-right">
                                                        <li><a href="#" data-toggle="modal" data-target="#invoice"><i class="icon-eye8"></i></a></li>
                                                        <li class="dropdown">
                                                            <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-menu7"></i> <span class="caret"></span></a>
                                                            <ul class="dropdown-menu dropdown-menu-right">
                                                                <li><a href="#"><i class="icon-printer"></i> Print invoice</a></li>
                                                                <li><a href="#"><i class="icon-file-download"></i> Download invoice</a></li>
                                                                <li class="divider"></li>
                                                                <li><a href="#"><i class="icon-file-plus"></i> Edit invoice</a></li>
                                                                <li><a href="#"><i class="icon-cross2"></i> Remove invoice</a></li>
                                                            </ul>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-lg-6">
                                            <div class="panel border-left-lg border-left-success invoice-grid timeline-content">
                                                <div class="panel-body">
                                                    <div class="row">
                                                        <div class="col-sm-6">
                                                            <h6 class="text-semibold no-margin-top">Rebecca Manes</h6>
                                                            <ul class="list list-unstyled">
                                                                <li>Invoice #: &nbsp;0027</li>
                                                                <li>Issued on: <span class="text-semibold">2015/02/24</span></li>
                                                            </ul>
                                                        </div>

                                                        <div class="col-sm-6">
                                                            <h6 class="text-semibold text-right no-margin-top">$5,100</h6>
                                                            <ul class="list list-unstyled text-right">
                                                                <li>Method: <span class="text-semibold">Paypal</span></li>
                                                                <li class="dropdown">
                                                                    Status: &nbsp;
                                                                    <a href="#" class="label bg-success-400 dropdown-toggle" data-toggle="dropdown">Paid <span class="caret"></span></a>
                                                                    <ul class="dropdown-menu dropdown-menu-right">
                                                                        <li><a href="#"><i class="icon-alert"></i> Overdue</a></li>
                                                                        <li><a href="#"><i class="icon-alarm"></i> Pending</a></li>
                                                                        <li class="active"><a href="#"><i class="icon-checkmark3"></i> Paid</a></li>
                                                                        <li class="divider"></li>
                                                                        <li><a href="#"><i class="icon-spinner2 spinner"></i> On hold</a></li>
                                                                        <li><a href="#"><i class="icon-cross2"></i> Canceled</a></li>
                                                                    </ul>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="panel-footer">
                                                    <ul>
                                                        <li><span class="status-mark border-success position-left"></span> Due: <span class="text-semibold">2015/03/24</span></li>
                                                    </ul>

                                                    <ul class="pull-right">
                                                        <li><a href="#" data-toggle="modal" data-target="#invoice"><i class="icon-eye8"></i></a></li>
                                                        <li class="dropdown">
                                                            <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-menu7"></i> <span class="caret"></span></a>
                                                            <ul class="dropdown-menu dropdown-menu-right">
                                                                <li><a href="#"><i class="icon-printer"></i> Print invoice</a></li>
                                                                <li><a href="#"><i class="icon-file-download"></i> Download invoice</a></li>
                                                                <li class="divider"></li>
                                                                <li><a href="#"><i class="icon-file-plus"></i> Edit invoice</a></li>
                                                                <li><a href="#"><i class="icon-cross2"></i> Remove invoice</a></li>
                                                            </ul>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- /invoices -->

                            </div>
                        </div>
                        <!-- /timeline -->
                    </div>

                    <div class="tab-pane fade in active" id="settings">

                        <!-- Profile info -->
                        <div class="panel panel-flat">
                            <div class="panel-heading">
                                <h6 class="panel-title">Chỉnh sửa thông tin</h6>
                                <div class="heading-elements">
                                    <ul class="icons-list">
                                        <li><a data-action="collapse"></a></li>
                                        <li><a data-action="reload"></a></li>
                                        <li><a data-action="close"></a></li>
                                    </ul>
                                </div>
                            </div>
                            <div class="panel-body">
                                <?php echo form_open('aff/profile'); ?>
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <label>Họ:</label>
                                            <?php echo form_input('firstName', $user['firstName'], 'class="form-control"') ?>
                                            <?php echo form_error('firstName') ?>
                                        </div>
                                        <div class="col-md-6">
                                            <label>Tên và tên đệm</label>
                                            <?php echo form_input('lastName', $user['lastName'], 'class="form-control"') ?>
                                            <?php echo form_error('lastName') ?>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <label>Giới tính:</label>

                                            <?php
                                            echo form_dropdown('sex', array('Nữ', 'Nam'), $user['sex'], 'class="select"')
                                            ?>

                                        </div>
                                        <div class="col-md-6">
                                            <label>Email:</label>
                                            <?php echo form_input('email', $user['email'], 'class="form-control" readonly="readonly"') ?>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <label>Điện thoại:</label>
                                            <?php echo form_input('phone', $user['phone'], 'class="form-control"') ?>
                                            <?php echo form_error('phone') ?>
                                        </div>
                                        <div class="col-md-6">
                                            <label>Tài khoản ngân hàng:</label>
                                            <?php echo form_input('paymen', $user['paymen'], 'class="form-control"') ?>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-md-4">
                                            <label>Xã/Phường:</label>
                                            <?php echo form_input('ward', $user['ward'], 'class="form-control"') ?>
                                            <?php echo form_error('ward') ?>
                                        </div>
                                        <div class="col-md-4">
                                            <label>Huyện/Quận:</label>
                                            <?php echo form_input('district', $user['district'], 'class="form-control"') ?>
                                            <?php echo form_error('district') ?>
                                        </div>
                                        <div class="col-md-4">
                                            <label>Tỉnh/Thành phố:</label>
                                            <?php echo form_input('city', $user['city'], 'class="form-control"') ?>
                                            <?php echo form_error('city') ?>
                                        </div>
                                    </div>
                                </div>

                                <div class="text-right">
                                    <button type="submit" class="btn btn-primary">Save <i class="icon-arrow-right14 position-right"></i></button>
                                </div>
                                <?php echo form_close(); ?>
                            </div>
                        </div>
                        <!-- /profile info -->


                        <!-- Account settings -->
                        <div class="panel panel-flat">
                            <div class="panel-heading">
                                <h6 class="panel-title">Thay đổi mật khẩu</h6>
                                <div class="heading-elements">
                                    <ul class="icons-list">
                                        <li><a data-action="collapse"></a></li>
                                        <li><a data-action="reload"></a></li>
                                        <li><a data-action="close"></a></li>
                                    </ul>
                                </div>
                            </div>

                            <div class="panel-body">
                                <div class="col-md-6">
                                    <?php echo form_open('aff/change_password'); ?>
                                    <div class="form-group">
                                        <div class="row">
                                            <div  class="inner">
                                                <label>Mật khẩu mới:</label>
                                                <?php echo form_password('password', '', 'class="form-control form-group" placeholder="Nhập mật khẩu mới"') ?>
                                            </div>
                                            
                                            <div style="margin-top: 20px;">
                                                <label>Nhập lại mật khẩu mới:</label>
                                                <?php echo form_password('passwordConfirm', '', 'class="form-control form-group" placeholder="Nhập lại mật khẩu mới cần thay đổi"') ?>
                                                <?php echo form_error('passwordConfirm') ?>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="text-left">
                                        <button type="submit" class="btn btn-primary">Lưu <i class="icon-arrow-right14 position-right"></i></button>
                                    </div>
                                    <?php echo form_close(); ?>
                                </div>
                                <div class="col-md-6">
                                    <?php echo form_open_multipart('aff/upload_avatar'); ?>
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-md-4">
                                                <?php if($_SESSION['loggedInUser']['avatar']): ?>
                                                    <img class="img-responsive" src="<?php echo base_url() . '/application/uploads/avatar/'.$user['avatar']; ?>">
                                                <?php else: ?>
                                                    <img src="<?php echo $this->template->dir() ?>assets/images/placeholder.jpg" class="img-responsive" alt=""/>
                                                <?php endif;?>
                                            </div>
                                            <div class="col-md-8">
                                                <label>Thay đổi ảnh đại diện:</label>
                                                <input name="avatar" type="file" class="file-styled">
                                                <input type="hidden" name="hiddenfile">
                                                <span class="help-block">Accepted formats: gif, png, jpg. Max file size 2Mb</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="text-right">
                                        <button type="submit" class="btn btn-primary">Lưu <i class="icon-arrow-right14 position-right"></i></button>
                                    </div>
                                    <?php echo form_close(); ?>
                                </div>
                            </div>
                        </div>
                        <!-- /account settings -->

                    </div>
                </div>
            </div>
        </div>

        <div class="col-lg-3">

            <!-- User thumbnail -->
            <div class="thumbnail">
                <div class="thumb thumb-rounded thumb-slide">
                    <?php if($_SESSION['loggedInUser']['avatar']): ?>
                        <img src="<?php echo base_url() . '/application/uploads/avatar/'.$user['avatar']; ?>">
                    <?php else: ?>
                        <img src="<?php echo $this->template->dir() ?>assets/images/placeholder.jpg" alt="">
                    <?php endif; ?>
                    <div class="caption">
                        <span>
                            <a href="#" class="btn bg-success-400 btn-icon btn-xs" data-popup="lightbox"><i class="icon-plus2"></i></a>
                            <a href="user_pages_profile.html" class="btn bg-success-400 btn-icon btn-xs"><i class="icon-link"></i></a>
                        </span>
                    </div>
                </div>

                <div class="caption text-center">
                    <h6 class="text-semibold no-margin"><?php echo $user['firstName'] . ' ' . $user['lastName']; ?></h6>
                    <ul class="icons-list mt-15">
                        <li><a href="#" data-popup="tooltip" title="Google Drive"><i class="icon-google-drive"></i></a></li>
                        <li><a href="#" data-popup="tooltip" title="Twitter"><i class="icon-twitter"></i></a></li>
                        <li><a href="#" data-popup="tooltip" title="Github"><i class="icon-github"></i></a></li>
                    </ul>
                </div>
            </div>
            <!-- /user thumbnail -->


            <!-- Navigation -->
            <div class="panel panel-flat">
                <div class="panel-heading">
                    <h6 class="panel-title">Navigation</h6>
                    <div class="heading-elements">
                        <a href="#" class="heading-text">See all &rarr;</a>
                    </div>
                </div>

                <div class="list-group list-group-borderless no-padding-top">
                    <a href="#activity" data-toggle="tab" class="list-group-item"><i class="icon-user"></i> Thông tin của tôi</a>
                    <a href="#settings" data-toggle="tab" class="list-group-item"><i class="icon-cog3"></i> Chỉnh sửa thông tin</a>
                    <a href="getBooking" class="list-group-item"><i class="icon-tree7"></i> Booking</a>
                    <a href="#" class="list-group-item"><i class="icon-users"></i> Friends</a>
                    <div class="list-group-divider"></div>
                    <a href="#" class="list-group-item"><i class="icon-calendar3"></i> Events <span class="badge bg-teal-400 pull-right">48</span></a>
                    <a href="#" class="list-group-item"><i class="icon-cog3"></i> Account settings</a>
                </div>
            </div>
            <!-- /navigation -->


            <!-- Share your thoughts -->
            <div class="panel panel-flat">
                <div class="panel-heading">
                    <h6 class="panel-title">Share your thoughts</h6>
                    <div class="heading-elements">
                        <ul class="icons-list">
                            <li><a data-action="close"></a></li>
                        </ul>
                    </div>
                </div>

                <div class="panel-body">
                    <form action="#">
                        <div class="form-group">
                            <textarea name="enter-message" class="form-control mb-15" rows="3" cols="1" placeholder="What's on your mind?"></textarea>
                        </div>

                        <div class="row">
                            <div class="col-sm-6">
                                <ul class="icons-list icons-list-extended mt-10">
                                    <li><a href="#" data-popup="tooltip" title="Add photo" data-container="body"><i class="icon-images2"></i></a></li>
                                    <li><a href="#" data-popup="tooltip" title="Add video" data-container="body"><i class="icon-film2"></i></a></li>
                                    <li><a href="#" data-popup="tooltip" title="Add event" data-container="body"><i class="icon-calendar2"></i></a></li>
                                </ul>
                            </div>

                            <div class="col-sm-6 text-right">
                                <button type="button" class="btn btn-primary btn-labeled btn-labeled-right">Share <b><i class="icon-circle-right2"></i></b></button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <!-- /share your thoughts -->


        </div>
    </div>
    <!-- /user profile -->

</div>

<!-- Theme JS files -->
<script type="text/javascript" src="<?php echo $this->template->dir() ?>assets/js/plugins/forms/selects/select2.min.js"></script>
<script type="text/javascript" src="<?php echo $this->template->dir() ?>assets/js/plugins/forms/styling/uniform.min.js"></script>
<script type="text/javascript" src="<?php echo $this->template->dir() ?>assets/js/plugins/ui/moment/moment.min.js"></script>
<script type="text/javascript" src="<?php echo $this->template->dir() ?>assets/js/plugins/ui/fullcalendar/fullcalendar.min.js"></script>
<script type="text/javascript" src="<?php echo $this->template->dir() ?>assets/js/plugins/visualization/echarts/echarts.js"></script>

<script type="text/javascript" src="<?php echo $this->template->dir() ?>assets/js/core/app.js"></script>
<script type="text/javascript" src="<?php echo $this->template->dir() ?>assets/js/pages/user_pages_profile.js"></script>
<!-- /theme JS files -->