<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Limitless - Responsive Web Application Kit by Eugene Kopyov</title>

        <!-- Global stylesheets -->
        <link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
        <link href="<?php echo $this->template->dir() ?>assets/css/icons/icomoon/styles.css" rel="stylesheet" type="text/css">
        <link href="<?php echo $this->template->dir() ?>assets/css/bootstrap.min.css" rel="stylesheet" type="text/css">
        <link href="<?php echo $this->template->dir() ?>assets/css/core.min.css" rel="stylesheet" type="text/css">
        <link href="<?php echo $this->template->dir() ?>assets/css/components.min.css" rel="stylesheet" type="text/css">
        <link href="<?php echo $this->template->dir() ?>assets/css/colors.min.css" rel="stylesheet" type="text/css">
        <!-- /global stylesheets -->

        <!-- Core JS files -->
        <script type="text/javascript" src="<?php echo $this->template->dir() ?>assets/js/plugins/loaders/pace.min.js"></script>
        <script type="text/javascript" src="<?php echo $this->template->dir() ?>assets/js/core/libraries/jquery.min.js"></script>
        <script type="text/javascript" src="<?php echo $this->template->dir() ?>assets/js/core/libraries/bootstrap.min.js"></script>
        <script type="text/javascript" src="<?php echo $this->template->dir() ?>assets/js/plugins/loaders/blockui.min.js"></script>
        <script type="text/javascript" src="<?php echo $this->template->dir() ?>assets/js/plugins/ui/nicescroll.min.js"></script>
        <script type="text/javascript" src="<?php echo $this->template->dir() ?>assets/js/plugins/ui/drilldown.js"></script>
        <!-- /core JS files -->

        <!-- Theme JS files -->
        <script type="text/javascript" src="<?php echo $this->template->dir() ?>assets/js/plugins/forms/styling/uniform.min.js"></script>

        <script type="text/javascript" src="<?php echo $this->template->dir() ?>assets/js/core/app.js"></script>
        <script type="text/javascript" src="<?php echo $this->template->dir() ?>assets/js/pages/login.js"></script>
        <!-- /theme JS files -->

    </head>

    <body class="bg-slate-800">

        <!-- Page container -->
        <div class="page-container login-container">

            <!-- Page content -->
            <div class="page-content">

                <!-- Main content -->
                <div class="content-wrapper">

                    <!-- Advanced login -->
                    <?php echo form_open() ?>
                    <div class="panel panel-body login-form">
                        <div class="text-center">
                            <div class="icon-object border-warning-400 text-warning-400"><i class="icon-people"></i></div>
                            <h5 class="content-group-lg">Đăng nhập tài khoản <small class="display-block">Điền thông tin tài khoản</small></h5>
                        </div>

                        <?php if (isset($_SESSION['error'])): ?>
                            <div class="form-group">
                                <div class="alert alert-danger">
                                    <?php echo $_SESSION['error'] ?>
                                </div>
                            </div>
                        <?php endif; ?>
                        <div class="form-group has-feedback has-feedback-left">
                            <input type="text" class="form-control" name="username" placeholder="Tài khoản">
                            <div class="form-control-feedback">
                                <i class="icon-user text-muted"></i>
                            </div>
                            <?php echo form_error('username') ?>
                        </div>

                        <div class="form-group has-feedback has-feedback-left">
                            <input type="password" class="form-control" name="password" placeholder="Mật khẩu">
                            <div class="form-control-feedback">
                                <i class="icon-lock2 text-muted"></i>
                            </div>
                            <?php echo form_error('password') ?>
                        </div>

                        <div class="form-group login-options">
                            <div class="row">
                                <div class="col-sm-6">
                                    <label class="checkbox-inline">
                                        <input type="checkbox" class="styled" checked="checked">
                                        Ghi nhớ
                                    </label>
                                </div>

                                <div class="col-sm-6 text-right">
                                    <a href="#">Quên mật khẩu?</a>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <button type="submit" class="btn bg-blue btn-block">Đăng nhập <i class="icon-circle-right2 position-right"></i></button>
                        </div>

                        <div class="content-divider text-muted form-group"><span>hoặc đăng nhập với</span></div>
                        <ul class="openid list-inline form-group list-inline-condensed text-center">
                            <li><a href="#" class="btn border-indigo text-indigo btn-flat btn-icon btn-rounded"><i class="icon-facebook"></i></a></li>
                            <li><a href="#" class="btn border-pink-300 text-pink-300 btn-flat btn-icon btn-rounded"><i class="icon-dribbble3"></i></a></li>
                            <li><a href="#" class="btn border-slate-600 text-slate-600 btn-flat btn-icon btn-rounded"><i class="icon-github"></i></a></li>
                            <li><a href="#" class="btn border-info text-info btn-flat btn-icon btn-rounded"><i class="icon-twitter"></i></a></li>
                        </ul>

                        <div class="content-divider text-muted form-group"><span>Chưa có tài khoản?</span></div>
                        <a href="<?php echo base_url('aff/register') ?>" class="btn bg-slate btn-block content-group">Đăng ký</a>
                        <span class="help-block text-center no-margin">Bằng việc đăng nhập bạn đồng ý với <a href="#"> Điều kiện </a> và <a href="#">Điều khoản Cookie</a></span>
                    </div>
                    <?php echo form_close() ?>
                    <!-- /advanced login -->

                </div>
                <!-- /main content -->

            </div>
            <!-- /page content -->


            <!-- Footer -->
            <div class="footer text-white">
                &copy; 2015. <a href="#">Gotdi Limitless</a> by <a href="#" target="_blank">HG Travel</a>
            </div>
            <!-- /footer -->

        </div>
        <!-- /page container -->
        <script>
            $(document).ready(function () {
                $('.openid a').click(function () {
                    alert('Hiện tại chúng tôi chưa cho phép đăng nhập bằng facebook, google, ...');
                });
            });
        </script>
    </body>
</html>
