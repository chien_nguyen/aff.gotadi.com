
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title><?php echo $template['title']?></title>
        <base href="<?php echo base_url()?>">
        <script>
            var themeBase = "<?php echo $this->template->dir() ?>";
        </script>
        <!-- Global stylesheets -->
        <link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
        <link href="<?php echo $this->template->dir() ?>assets/css/icons/icomoon/styles.css" rel="stylesheet" type="text/css">
        <link href="<?php echo $this->template->dir() ?>assets/css/bootstrap.min.css" rel="stylesheet" type="text/css">
        <link href="<?php echo $this->template->dir() ?>assets/css/core.min.css" rel="stylesheet" type="text/css">
        <link href="<?php echo $this->template->dir() ?>assets/css/components.min.css" rel="stylesheet" type="text/css">
        <link href="<?php echo $this->template->dir() ?>assets/css/colors.min.css" rel="stylesheet" type="text/css">
        <!-- /global stylesheets -->

        <!-- Core JS files -->
        <script type="text/javascript" src="<?php echo $this->template->dir() ?>assets/js/plugins/loaders/pace.min.js"></script>
        <script type="text/javascript" src="<?php echo $this->template->dir() ?>assets/js/core/libraries/jquery.min.js"></script>
        <script type="text/javascript" src="<?php echo $this->template->dir() ?>assets/js/core/libraries/bootstrap.min.js"></script>
        <script type="text/javascript" src="<?php echo $this->template->dir() ?>assets/js/plugins/loaders/blockui.min.js"></script>
        <script type="text/javascript" src="<?php echo $this->template->dir() ?>assets/js/plugins/ui/nicescroll.min.js"></script>
        <script type="text/javascript" src="<?php echo $this->template->dir() ?>assets/js/plugins/ui/drilldown.js"></script>
        <!-- /core JS files -->

        <script>
            var themeBase = "<?php echo $this->template->dir()?>";
        </script>

    </head>

    <body>

        <!-- Main navbar -->
        <?php require_once 'navbar.php';?>
        <!-- /main navbar -->


        <!-- Second navbar -->
        <?php require_once 'second-navbar.php';?>
        <!-- /second navbar -->


        <!-- Page header -->
        <?php require_once 'page-header.php';?>
        <!-- /page header -->


        <!-- Page container -->
        <div class="page-container">

            <!-- Page content -->
            <div class="page-content">

                <!-- Main content -->
                <?php echo $template['body']?>
                <!-- /main content -->

            </div>
            <!-- /page content -->


            <!-- Footer -->
            <?php require_once 'footer.php';?>
            <!-- /footer -->

        </div>
        <!-- /page container -->

    </body>
</html>
