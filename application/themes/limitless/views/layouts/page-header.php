<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-arrow-left52 position-left"></i> <?php echo $template['title']?></h4>

            <ul class="breadcrumb breadcrumb-caret position-right">
                <?php foreach ($template['breadcrumbs'] as $item) :?>
                <li><a href="<?php echo $item['uri']?>"><?php echo $item['name']?></a></li>
                <?php endforeach;?>
                <li class="active"><?php echo $template['title']?></li>
            </ul>
            

            
        </div>

        <div class="heading-elements">
            <div class="heading-btn-group">
                <a href="#" class="btn btn-link btn-float has-text"><i class="icon-bars-alt text-primary"></i><span>Báo cáo</span></a>
                <a href="#" class="btn btn-link btn-float has-text"><i class="icon-calculator text-primary"></i> <span>Hóa đơn</span></a>
                <a href="#" class="btn btn-link btn-float has-text"><i class="icon-calendar5 text-primary"></i> <span>Lịch</span></a>
            </div>
        </div>
    </div>
</div>

