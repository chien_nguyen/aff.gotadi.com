<div class="navbar navbar-inverse">
    <div class="navbar-header">
        <a class="navbar-brand" href="<?php echo base_url('aff') ?>"><img src="<?php echo $this->template->dir() ?>assets/images/logo_light.png" alt=""></a>

        <ul class="nav navbar-nav pull-right visible-xs-block">
            <li><a data-toggle="collapse" data-target="#navbar-mobile"><i class="icon-tree5"></i></a></li>
        </ul>
    </div>

    <div class="navbar-collapse collapse" id="navbar-mobile">


        <ul class="nav navbar-nav navbar-right">
            <li class="dropdown language-switch">
                <a class="dropdown-toggle" data-toggle="dropdown">
                    <img src="<?php echo $this->template->dir() ?>assets/images/flags/vn.png" class="position-left" alt="">
                    Tiếng Việt
                </a>
            </li>

            <li class="dropdown dropdown-user">
                <a class="dropdown-toggle" data-toggle="dropdown">
                    <img src="<?php echo $this->template->dir() ?>assets/images/placeholder.jpg" alt="">
                    <span><?php echo $_SESSION['loggedInUser']['firstName'] . ' ' . $_SESSION['loggedInUser']['lastName'] ?></span>
                    <i class="caret"></i>
                </a>

                <ul class="dropdown-menu dropdown-menu-right">
                    <li><a href="#"><i class="icon-user-plus"></i> Tài khoản</a></li>
                    <li><a href="#"><i class="icon-coins"></i> Số dư</a></li>
                    <li><a href="#"><span class="badge badge-default pull-right">0</span> <i class="icon-comment-discussion"></i> Tin nhắn</a></li>
                    <li class="divider"></li>
                    <li><a href="<?php echo base_url('aff/profile')?>"><i class="icon-cog5"></i>Cấu hình tài khoản</a></li>
                    <li><a href="<?php echo base_url('aff/logout') ?>"><i class="icon-switch2"></i> Thoát</a></li>
                </ul>
            </li>
        </ul>
    </div>
</div>