<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class View extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->template->set_theme('mobile')->set_layout('default');
    }

    public function index() {
        $this->template->build('flight/flight_home');
    }
    
    public function contact()
    {
        $this->template->build('flight/contact');
    }
    public function result()
    {
        $this->template->build('flight/result');
    }
    public function posts()
    {
        $this->template->build('post/list');
    }
    public function single()
    {
        $this->template->build('post/single');
    }
    

}
