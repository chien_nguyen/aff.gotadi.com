<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Admin_Controller extends MX_Controller {

    public function __construct() {

        parent::__construct();

        $this->load->library('template');

        $this->template->set_theme('admin');

        //Default Title
        $this->template->title('Atrip', 'Portal du lịch tập trung đầu tiên tại Việt Nam');

        //Load config file
        $this->config->load('setting');

        
        $this->form_validation->set_error_delimiters('<div class="text-danger">', '</div>');

        
        $uri = $this->uri->uri_string();
        
        //Kiểm tra đăng nhập
        if ($uri != 'admin/login') {
            if (!isset($this->session->userdata['admin_logged_in'])) {
                redirect('admin/login');
            }
        }
    }

}
