<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Reseller_Controller extends MX_Controller {

    public function __construct() {

        parent::__construct();

        $this->load->library('template');

        $this->template->set_theme('porto_admin');

        //Set Admin Default Layout
        $this->template->set_layout('main');

        //Default Title
        $this->template->title('Atrip', 'Portal du lịch tập trung đầu tiên tại Việt Nam');

        //Load config file
        $this->config->load('setting');

        
        $this->form_validation->set_error_delimiters('<div class="text-danger">', '</div>');

        
        $uri = $this->uri->uri_string();
        
        //Kiểm tra đăng nhập
        if ($uri != 'reseller/login') {
            if (!isset($this->session->userdata['reseller_logged_in'])) {
                redirect('reseller/login');
            }
        }
    }

}
