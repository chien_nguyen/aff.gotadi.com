<?php

/* 
 * Chào mừng các bạn đến với diễn đàn codeigntier việt nam - Codeigniter việt nam, diễn đàn lập trình, diễn đàn codeiginiter
 * Các bài viết được thực hiện từ diễn đàn nếu các bạn re public vui lòng ghi rõ nguồn
 * Hoàn toàn miễn phí, vì vậy các bạn hãy tôn trọng quyền tác giả đã làm ra bài này.
 * Thực hiện: Tinhphaistc.
 */

if (!defined('BASEPATH')) exit('No direct script access allowed');

class MY_Form_validation extends CI_Form_validation
{
 function run($module = '', $group = '') {
 (is_object($module)) AND $this->CI =& $module;
 return parent::run($group);
 }
}
/* End of file MY_Form_validation.php */
/* Location: ./application/libraries/MY_Form_validation.php */