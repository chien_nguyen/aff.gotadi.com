<?php

/**
 * class Aws
 */
require_once dirname(dirname(__FILE__)) . '/third_party/aws/aws-autoloader.php';

use Aws\S3\MultipartUploader;
use Aws\Exception\MultipartUploadException;

class Aws {

    public $client;
    public $bucket;
    public $bucketBase;

    public function __construct() {
        $this->client = new Aws\S3\S3Client([
            'version' => 'latest',
            'region' => 'ap-southeast-1',
            'ssl.certificate_authority' => false,
            'credentials' => [
                'key' => 'AKIAJRUXGR5NCQJWQJWA',
                'secret' => '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'
            ],
        ]);

        $this->bucket = 'image.atrip.vn';
        $this->bucketBase = 's3://image.atrip.vn/';
    }

    public function listBucket() {
        $this->client->listBuckets();
    }

    public function upload($source, $dest_full_path) {


        $uploader = new MultipartUploader($this->client, $source, [
            'bucket' => $this->bucket,
            'key' => $dest_full_path,
        ]);


        try {
            $uploader->upload();
            return true;
        } catch (MultipartUploadException $e) {

            echo $e->getMessage() . "\n";

            return false;
        }
    }

    function save_image($inPath, $outPath) { //Download images from remote server
        ini_set("user_agent", "Testing for http://stackoverflow.com/questions/5509640");
        $in = fopen($inPath, "rb");
        $out = fopen($outPath, "wb");
        while ($chunk = fread($in, 8192)) {
            fwrite($out, $chunk, 8192);
        }
        fclose($in);
        fclose($out);
    }

}
