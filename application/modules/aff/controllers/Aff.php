<?php

defined('BASEPATH') or exit('Không được quyền truy cập');

class Aff extends MX_Controller {

    public function __construct() {
        parent::__construct();
        $this->template->set_theme('limitless');
        $this->form_validation->set_error_delimiters('<span class="help-block text-danger"><i class="icon-cancel-circle2 position-left"></i>', '</span>');
        $this->load->model('user_m');
        
        if($this->router->fetch_method() != 'login' && $this->router->fetch_method() != 'register'){
            if(!isset($_SESSION['loggedInUser'])){
                redirect('aff/login');
            }
        }
        $this->template->set_breadcrumb('Trang đầu', base_url('aff'));

    }

    public function index() {
        $this->template->title('Dashboard')
                ->build('aff/index');
    }

    public function login() {
        if (isset($_SESSION['loggedInUser'])) {
            redirect('aff/index');
        }

        $rules = array(
            array(
                'field' => 'username',
                'label' => 'Tên đăng nhập',
                'rules' => 'trim|required|callback__valid_username'
            ),
            array(
                'field' => 'password',
                'label' => 'Mật khẩu',
                'rules' => 'trim|required'
            ),
        );

        $this->form_validation->set_rules($rules);
        if ($this->form_validation->run($this)) {
            $result = $this->user_m->login($_POST['username'], $_POST['password']);
            if ($result != false) {
                $this->session->set_userdata('loggedInUser', $result);
                redirect('aff/getBooking');
            } else {
                $this->session->set_flashdata('error', 'Đăng nhập không thành công, tài khoản hoặc mật khẩu ko đúng');
                redirect('aff/login');
            }
        } else {

            $this->template->set_layout('empty')
                    ->build('aff/login');
        }
    }

    public function register() {
        if (isset($_SESSION['loggedInUser'])) {
            $this->session->sess_destroy();
        }

        $rules = array(
            array(
                'field' => 'username',
                'label' => 'Tên đăng nhập',
                'rules' => 'trim|required|callback__valid_username'
            ),
            array(
                'field' => 'password',
                'label' => 'Mật khẩu',
                'rules' => 'trim|required'
            ),
            array(
                'field' => 'passwordConfirm',
                'label' => 'Xác nhận mật khẩu',
                'rules' => 'trim|required|matches[password]'
            ),
            array(
                'field' => 'email',
                'label' => 'Địa chỉ Email',
                'rules' => 'trim|required|valid_email'
            ),
            array(
                'field' => 'emailConfirm',
                'label' => 'Xác nhận Email',
                'rules' => 'trim|required|matches[email]'
            ),
            array(
                'field' => 'firstName',
                'label' => 'Họ',
                'rules' => 'trim|required'
            ),
            array(
                'field' => 'lastName',
                'label' => 'Tên đệm và tên',
                'rules' => 'trim|required'
            ),
            array(
                'field' => 'acceptTerm',
                'label' => 'Điều khoản sử dụng',
                'rules' => 'callback__checkAcceptTerm'
            ),
        );

        $user = array();

        if (count($_POST) == 0) {
            foreach ($rules as $value) {
                $user[$value['field']] = '';
            }
            $user['acceptTerm'] = 1;
        } else {
            foreach ($_POST as $key => $value) {
                $user[$key] = $value;
            }
            if (!isset($_POST['acceptTerm'])) {
                $user['acceptTerm'] = 1;
            }
        }

        $this->form_validation->set_rules($rules);
        if ($this->form_validation->run($this)) {
            unset($_POST['acceptTerm']);
            unset($_POST['subcribe']);
            unset($_POST['emailConfirm']);
            unset($_POST['passwordConfirm']);
            ksort($_POST);
            $_POST['password'] = md5($_POST['password']);
            $result = $this->user_m->insert($_POST);
            if ($result != FALSE) {
                $this->session->set_userdata('loggedInUser', $this->user_m->get($result));
                $this->session->set_flashdata('success', 'Chúc mừng bạn đã đăng ký tái khoản thành công');
                redirect('aff/profile');
            } else {
                $this->session->set_flashdata('error', 'Có lỗi xảy ra trong quá trình đăng ký, xin vui lòng thử lại sau');
                redirect('aff/register');
            }
        } else {
            $this->template->set_layout('empty')
                    ->build('aff/register', $user);
        }
    }

    public function profile() {
        $this->template->title('Cấu hình tài khoản')
                ->set_breadcrumb('Tài khoản', base_url('aff/profile'));
        $rules = array(
            array(
                'field' => 'firstName',
                'label' => 'Họ',
                'rules' => 'required'
            ),
            array(
                'field' => 'lastName',
                'label' => 'Họ và tên đệm',
                'rules' => 'required'
            ),
            array(
                'field' => 'sex',
                'label' => 'Giới tính',
                'rules' => 'required'
            ),
            array(
                'field' => 'ward',
                'label' => 'Xã/Phường',
                'rules' => 'trim'
            ),
            array(
                'field' => 'district',
                'label' => 'Huyện/Quận',
                'rules' => 'trim'
            ),
            array(
                'field' => 'city',
                'label' => 'Tỉnh/Thành phố',
                'rules' => 'trim'
            ),
            array(
                'field' => 'phone',
                'label' => 'Số điện thoại',
                'rules' => 'trim'
            )
        );
        if (count($_POST) == 0) {
            foreach ($rules as $value) {
                $user[$value['field']] = '';
            }
        } else {
            foreach ($_POST as $key => $value) {
                $user[$key] = $value;
            }
        }        
        $this->form_validation->set_rules($rules);
        if ($this->form_validation->run()) {
            unset($_POST['passwordConfirm']);
            ksort($_POST);
            $_POST['password'] = md5($_POST['password']);
            if($this->user_m->update($_SESSION['loggedInUser']['id'], $_POST)){
                $this->session->set_flashdata('success', 'Cập nhật thành công');
            }else{
                $this->session->set_flashdata('error', 'Cập nhật thất bại, xin thử lại sau');
            }
            redirect('aff/profile');
            
        } else {
            $data['user'] = $this->user_m->get($_SESSION['loggedInUser']['id']);
            $this->template->set('user', $data['user'])->build('aff/profile');
        }
    }
    
    
    /**
     * 
     */
    public function change_password(){
         $rules = array(
            array(
                'field' => 'password',
                'label' => 'Mật khẩu',
                'rules' => 'trim|required'
            ),
            array(
                'field' => 'passwordConfirm',
                'label' => 'Xác nhận mật khẩu',
                'rules' => 'trim|required|matches[password]'
            )
        );
        if (count($_POST) == 0) {
            foreach ($rules as $value) {
                $user[$value['field']] = '';
            }
        } else {
            foreach ($_POST as $key => $value) {
                $user[$key] = $value;
            }
        }
        $this->form_validation->set_rules($rules);
        if ($this->form_validation->run()) {
            unset($_POST['passwordConfirm']);
            ksort($_POST);
            $_POST['password'] = md5($_POST['password']);
            if($this->user_m->change_passwords($_SESSION['loggedInUser']['id'], $_POST)){
                $this->session->set_flashdata('success', 'Thay đổi mật khẩu thành công');
            }else{
                $this->session->set_flashdata('error', 'Thay đổi thất bại');
            }
            redirect('aff/profile');
            
        } else {
            $data['user'] = $this->user_m->get($_SESSION['loggedInUser']['id']);
            $this->template->set('user', $data['user'])->build('aff/profile');
        }
    }
    
    
    /**
     * 
     * @param type $str
     * @return boolean
     */
    function _valid_username($str)
    {
        if ( ! preg_match('/^[a-zA-Z0-9_.]*$/', $str) )
        {
            // Set the error message:
            $this->form_validation->set_message('_valid_username', 'Tên đăng nhập chỉ được dùng ký tự chữ, số và dấu chấm');
            return FALSE;
        }
        else
        {
            return TRUE;
        }
    }

    public function logout() {
        $this->session->sess_destroy();
        redirect('aff/login');
    }

    public function getBooking() {
        $user = $_SESSION['loggedInUser']['username'];
        $this->load->model('affiliate_m');
        $result = $this->affiliate_m->getBooking($user, date('Y-m-d'), date('Y-m-d'));
        $month_books = $this->affiliate_m->getBooking($user, date('Y-m-') . '01', date('Y-m-d'));
        $this->template->set('month_books', $month_books['GetBookingsByAffiliateResult'])->set('bookings', $result['GetBookingsByAffiliateResult'])->build('aff/bookings');
    }

    public function view_session() {
        echo '<pre><xmp>';
        print_r($_SESSION);
        echo '</xmp></pre>';
    }

    function _checkAcceptTerm() {
        if (isset($_POST['acceptTerm']) && $_POST['acceptTerm'] == 1) {
            return TRUE;
        } else {
            $this->form_validation->set_message('_checkAcceptTerm', 'Bạn chưa đồng ý với điều khoản sử dụng');
        }
    }
    
    public function upload_avatar() {
        $this->form_validation->set_rules('hiddenfile', 'Submit', 'callback__validate_file_required');
        if ($this->form_validation->run($this)) {
                $path = dirname(BASEPATH) . '/application/uploads/avatar/';
                $tmp_name = $_FILES['avatar']['tmp_name'];
                $name = $_FILES['avatar']['name'];
                $array = explode('.', $name);
                $n = count($array);
                $name_file = md5(microtime().  rand(1,1000));
                $new_name = $name_file.'.'.$array[$n-1];
                // Upload file
                move_uploaded_file($tmp_name, $path . $new_name);
                
                if($this->user_m->change_avatar($_SESSION['loggedInUser']['id'] , array('avatar'=>$new_name))){
                    $this->session->set_flashdata('success', 'Cập nhật avatar thành công');
                }else{
                    $this->session->set_flashdata('error', 'Cập nhật thất bại, xin thử lại sau');
                }
                $this->reload_session();
                redirect('aff/profile');
            }
       else {
            $data['user'] = $this->user_m->get($_SESSION['loggedInUser']['id']);
            $this->template->set('user', $data['user'])->build('aff/profile');
        }
    }
    private function reload_session() {
        $id = $_SESSION['loggedInUser']['id'];
        $result = $this->user_m->get($id);
        $this->session->set_userdata('loggedInUser', $result);
    }
    public function _validate_file_required()
    {
        
    }
    
    function validation_file() {
        if ($_FILES['img_upload']['error'][0] == 0) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

}
