<?php

class Affiliate {

    private $_AffiliateID;
    private $_FromDate;
    private $_ToDate;
    private $_url;
    private $_client;
    private $_soap_options = array(
        'soap_version' => SOAP_1_1,
        'exceptions' => true,
        'encoding' => 'UTF-8',
        'content-type' => 'text/xml'
    );

    public function __construct() {
        $this->_url = $this->config->item('bookingURL');
        try {
            $this->_client = new SoapClient($this->_url, $this->_soap_options);
        } catch (SoapFault $ex) {
            return false;
        };
    }

    public function setID($value) {
        $this->_AffiliateID = $value;
        return $this;
    }

    public function setFromDate($value) {
        $this->_FromDate = $data . 'T00:00:00.000+07:00';
        return $this;
    }

    public function setToDate($value) {
        $this->_ToDate = $data . 'T00:00:00.000+07:00';
        return $this;
    }

    public function getBookingByAffiliate() {
        
    }

}
