<?php

defined('BASEPATH') or exit('Không được quyền truy cập');

class Affiliate_m extends CI_Model {

    private $_url;
    private $_client;
    private $_soap_options = array(
        'soap_version' => SOAP_1_1,
        'exceptions' => true,
        'encoding' => 'UTF-8',
        'content-type' => 'text/xml'
    );

    public function __construct() {
        parent::__construct();

        $this->_url = $this->config->item('bookingURL');
        try {
            $this->_client = new SoapClient($this->_url, $this->_soap_options);
        } catch (SoapFault $ex) {
            return false;
        }
    }

    public function getBooking($affiliateID, $from, $to) {
        $params = array(
            'AffiliateID' => $affiliateID,
            'FromDate' => $from . 'T00:00:00.000+07:00',
            'ToDate' => $to . 'T00:00:00.000+07:00',
        );
        try {
            $result = $this->_client->GetBookingsByAffiliate($params);
            if ($result) {
                if(count($result->GetBookingsByAffiliateResult)  == 0){
                    return FALSE;
                }else{
                    return json_decode(json_encode($result), true);;
                }
                
            } else {
                return false;
            }
        } catch (SoapFault $ex) {
            return false;
        }
    }

}
