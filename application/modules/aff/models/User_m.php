<?php

defined('BASEPATH') or exit('Không được quyền truy cập');

class User_m extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    public function insert($input, $likeField = true) {
        $this->db->insert('users', $input);
        if ($this->db->insert_id() > 0) {
            return $this->db->insert_id();
        }
        return false;
    }
    
    public function get($id)
    {
        $query = $this->db->where('id', $id)->get('users');
        if($query->num_rows() > 0){
            return $query->row_array();
        }
        return false;
    }
    
    public function login($username, $password){
        $query = $this->db->where('username', $username)
                ->where('password', md5($password))->get('users');
        if($query->num_rows() == 0){
            return false;
        }
        return $query->row_array();
    }
    //Update thông tin trong profile
    public function update($id, $input) {
        $this->db->where('id', $id)
                ->update('users', $input);
        if($this->db->affected_rows() > 0){
            return true;
        }
        return false;
    }
    //Thay đổi mật khẩu
    public function change_passwords($id, $input){
        $this->db->where('id', $id)
                ->update('users', $input);
        if($this->db->affected_rows() > 0){
            return true;
        }
        return false;
    }
    //Thay dổi avatar
    public function change_avatar($id, $input){
        $this->db->where('id', $id)
                ->update('users', $input);
    }
}
