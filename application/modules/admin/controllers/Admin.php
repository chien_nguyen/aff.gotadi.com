<?php

defined('BASEPATH') or exit('Không được quyền truy cập');

class Admin extends Admin_Controller {

    public function __construct() {
        parent::__construct();
    }

    public function index() {
        $this->template->set_layout('dashboard')
                ->build('admin/index');
    }

    public function login() {
        $rules = array(
            array(
                'field' => 'username',
                'label' => 'Username',
                'rules' => 'required|min_length[6]|max_length[32]'
            ),
            array(
                'field' => 'password',
                'label' => 'Password',
                'rules' => 'required|min_length[6]|max_length[32]'
            ),
        );

        $this->form_validation->set_rules($rules);
        if ($this->form_validation->run()) {
           
            if($_POST['username'] == 'gotadi' and $_POST['password'] == 'Gotadi@')
            {
                $this->session->set_userdata('admin_logged_in', 1);
                redirect('admin/index');
            }else{
                $this->session->set_flashdata('error', 'Sai mật khẩu');
                redirect('admin/login');
            }
            
            
        } else {
            $this->template->set_layout('login')
                    ->build('login');
        }
    }

}
