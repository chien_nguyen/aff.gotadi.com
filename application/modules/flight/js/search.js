$(function () {
    $.xhrPool = [];
    $.xhrPool.abortAll = function () {
        $(this).each(function (i, jqXHR) {   //  cycle through list of recorded connection
            jqXHR.abort();  //  aborts connection
            $.xhrPool.splice(i, 1); //  removes from list by index
        });
    }
    $.ajaxSetup({
        beforeSend: function (jqXHR) {
            $.xhrPool.push(jqXHR);
        }, //  annd connection to list
        complete: function (jqXHR) {
            var i = $.xhrPool.indexOf(jqXHR);   //  get index for current connection completed
            if (i > -1)
                $.xhrPool.splice(i, 1); //  removes from list by index
        }
    });
})


function compare(a, b) {
    if (a.FarePrice.Adult.FareCost < b.FarePrice.Adult.FareCost)
        return -1;
    if (a.FarePrice.Adult.FareCost > b.FarePrice.Adult.FareCost)
        return 1;
    return 0;
}

function list_flights(flights, brand, way) {
//
    var n = 0;
    if (way === 'outbound') {
        $('.list-flights.outbound').html('');
    } else {
        $('.list-flights.inbound').html('');
    }

    $.each(flights, function (index, value) {

        if ($('.list-flights.outbound').length > 0) {
            $('.way-ticket').show(0);
        }

        var classs;
        if (value.B == 'js')
        {
            classs = 'Eco-' + value.ClassOfService;
        } else {
            classs = value.ClassOfService;
        }

        if (value.B == 'vj') {
            if (value.ClassOfService == 'Eco') {
                classs = 'Economy'
            }
        }

        var html =
                '<li data-brand="' + value.B + '" id="' + value.id + '" class="item ' + value.B + '">\n\
                                      <div class="col-xs-3 b">\n\
                       <span class="duration"><img src="' + value.IconName + '"/></span>\n\
                       <div class="class">' + classs + '</div>\n\
                   </div>\n\
                   <div class="col-xs-5 b">\n\
                       <span class="time">' + value.STD + ' - ' + value.STA + '</span>\n\
                       <div class="small">' + value.FlightNumber + ' - Bay thẳng</div>\n\
                   </div>\n\
                    <div class="col-xs-4 b">\n\
                       <span class="price">' + $.number(value.FarePrice.Total.FareCost, 0, '.') + ' <span class="small">đ</span></span>\n\
                       <div class="small">Giá cho ' + (totalTicket) + ' vé</div>\n\
                   </div>\n\
                   <div class="flight-detail">\n\
                        <div class="inner">\n\
                        <div class="col-xs-12 classname">' + classs + ' <span>Class</span></div>\n\
                        <div class="flight-info">\n\
                            <div class="ticket-logo"><img src="' + value.Logo + '" width="150"/></div>\n\
                            <div class="city">\n\
                                <div class="col-xs-6 text-left">\n\
                                    <div class="city-name">' + value.dcity + '</div>\n\
                                    <div class="city-code">' + value.DepartureStation + '</div>\n\
                                </div>\n\
                                <div class="col-xs-6 text-right">\n\
                                    <div class="city-name">' + value.acity + '</div>\n\
                                    <div class="city-code">' + value.ArrivalStation + '</div>\n\
                                </div>\n\
                            </div>\n\
                            <div class="flight-date">\n\
                                ' + value.DDATE + '\n\
                            </div>\n\
                            <div class="flight-time">\n\
                                <div class="col-xs-6 text-center">\n\
                                    <div class="departure-time time">' + value.STD + '</div>\n\
                                </div>\n\
                                <div class="col-xs-6 text-center">\n\
                                    <div class="arrival-time time">' + value.STA + '</div>\n\
                                </div>\n\
                            </div>\n\
                            </div>\n\
                        <ul class="list-unstyled flight-fare col-xs-12">\n\
                            <li>Giá vé: <span class="pull-right">' + $.number(value.FarePrice.Total.FareCost, 0, '.') + '</span></li>\n\
                            <li>Phí và thuế: <span class="pull-right">' + $.number(value.FarePrice.Total.TotalFee, 0, '.') + '</span></li>\n\
                            <li class="price">Tổng cộng: <span class="pull-right">' + $.number(value.FarePrice.Total.Total, 0, '.') + '</span></li>\n\
                            <li class="button-select-flight row">\n\
                            <div class="col-sm-12"><button class="select-ticket btn btn-block">Chọn vé này</button></div></li>\n\
                                <div class="col-sm-12"><a class="close-item" href="#">Chọn chuyến khác</a> </div> \n\
                        </ul>\n\
                   </div>\n\
                   </div>\n\
               </li>';


        if (value.ClassOfService != 'SkyBoss') {
            if (way === 'outbound') {
                $('.list-flights.outbound').append(html);
            } else {
                $('.list-flights.inbound').append(html);
            }
        }

        $('#' + value.id).find('.close-item').click(function (e) {
            e.preventDefault();
            $(this).closest('.flight-detail').hide(0);

            $('.white-bg').fadeOut('150');
        });


        $('#' + value.id).find('.b').click(function () {
            $('#' + value.id).find('.flight-detail').slideToggle(0);
            $('.white-bg').show(0);

        });
        if (index > 15)
        {
            $('#' + value.id).addClass('hide');
        }
        select_inbound(value.id);
        select_outbound(value.id);
    });

    $('.white-bg').hide();

    if (way === 'outbound') {
        showall_button('outbound');
    } else {
        showall_button('inbound');
    }
}

function showall_button(way) {
    var list = $('.list-flights.' + way + ' .item');
    var length = list.length;
    if (way === 'inbound')
    {
        var string = 'về';
    } else {
        var string = 'đi';
    }

    if (length > 15) {
        var next = length - 15;
        var xhtml = '<li class="item text-center text-success" id="show-all-' + way + '">Xem thêm ' + next + ' vé lượt ' + string + ' ...</li>';
        if ($('#show-all-' + way).length === 0) {
            $('.list-flights.' + way).append(xhtml);
        } else {
            $('#show-all-' + way).replaceWith(xhtml);
        }
        $('#show-all-' + way).click(function () {
            $('.list-flights.' + way + ' .item').removeClass('hide');
            $(this).hide();
        });
    }
}


//Chiều về
function select_inbound(id)
{
    var base = $('base').attr('href');
    var brand = $('#' + id).data('brand');
    $('.list-flights.inbound #' + id + ' button').click(function () {
        $.ajax({
            url: base + 'flight/select_inbound/' + id + '/' + brand,
            success: function (data) {
                $('.booking-inbound').replaceWith(data);
                update_total_fare();

                $.ajax({
                    url: base + 'flight/check_selected',
                    success: function (res)
                    {
                        //Kiểm tra xem đã chọn chiều đi chưa
                        if (res == 1) {
                            $('.flight-detail').hide();
                            $('html,body').animate({
                                scrollTop: $('.booking-infomartion').offset().top - 52
                            }, 1000);
                            //Nếu chưa chọn thì quay lại chọn chiều đi
                        } else {
                            $('.flight-detail').hide();
                            $('html,body').animate({
                                scrollTop: $('.way-ticket.outbound').offset().top - 52
                            }, 1000);
                        }
                    }
                });



            },
            complete: function () {
            }
        });

        $('.white-bg').hide(0);
    });
}

function select_outbound(id)
{
    var base = $('base').attr('href');
    var brand = $('#' + id).data('brand');
    $('.list-flights.outbound #' + id + ' button').click(function () {
        var vBrand = $(this).closest('#' + id).data('brand');
        $('.list-flights.inbound>li').hide(0);
        $('.list-flights.inbound .item.' + vBrand).removeClass('hide').show(0);

        $.ajax({
            url: base + 'flight/select_outbound/' + id + '/' + brand,
            success: function (data) {
                $('.booking-infomartion').show(0);
                $('.search-banner').hide(0);

                $('.booking-outbound').replaceWith(data);
                update_total_fare();

                if (roundtype === 'roundtrip') {


                    //Kiểm tra xem chọn chiều về chưa
                    $.ajax({
                        url: base + 'flight/check_selected/inbound',
                        success: function (res)
                        {
                            if (res == 1) {
                                $('html,body').animate({
                                    scrollTop: $('.booking-infomartion').offset().top - 52
                                }, 500);
                                //Nếu chưa chọn thì xuống chọn chiều về
                            } else {
                                $('html,body').animate({
                                    scrollTop: $('.way-ticket.inbound').offset().top - 52
                                }, 500);
                            }

                        }
                    });
                    $('.flight-detail').hide();
                    $('html,body').animate({
                        scrollTop: $('.way-ticket.inbound').offset().top
                    }, 500);
                } else {
                    $('.flight-detail').hide();
                    $('html,body').animate({
                        scrollTop: $('.booking-infomartion').offset().top
                    }, 500);
                }
            }
        });
        $('.white-bg').hide(0);
    });
}

function update_total_fare() {
    var base = $('base').attr('href');
    $.ajax({
        url: base + 'flight/update_total_fare',
        success: function (data) {
            $('.totalFare').text($.number(data, 0, '.'));
        }
    });
}

$(".progressBar").ajaxSend(function (r, s) {
    $(this).show();
    console.log(r, s);
});

$(".progressBar").ajaxComplete(function (r, s) {
    $(this).hide();
    console.log(r);
    console.log(s);
});
$(document).ready(function () {
    var base = $('base');
    $('.show-flight').click(function (e) {
        e.preventDefault();
        $('.list-flight li').css('display', 'block');
        $(this).hide();
    })

    var outbound = [];
    var inbound = [];
    var base = $('base').attr('href');


    //Trường hợp này khách chọn tất cả các hãng bay
    if (airlines === 'all') {
        get_flights('jetstar', 'vna', 'vietjet');

        //Nếu khách chỉ chọn 1 hãng Vietnam Airlines
    } else if (airlines === 'vn') {
        $.when(get_vna(60)).done(function () {

        });

        //Chỉ chọn Vietjet
    } else if (airlines === 'vj') {
        $.when(get_vietjet(60)).done(function () {
            //finish_progress();
        });

        //Chỉ chọn jetstar
    } else if (airlines === 'js') {
        $.when(get_jetstar(60)).done(function () {
            //finish_progress();
        });
    }



    function get_flights(j, k, l)
    {
        $.ajax({
            url: base + 'flight/' + j,
            success: function (data) {
                if (data != 'fail') {
                    finish_search(data, j);
                }
            },
            complete: function () {
                $('.booking-infomartion').show(0);
                $('.search-banner').hide(0);
                if (k != undefined) {
                    $.ajax({
                        url: base + 'flight/' + k,
                        success: function (data) {
                            if (data != 'fail') {
                                finish_search(data, l);
                            }
                        },
                        complete: function () {
                            if (l != undefined) {
                                $.ajax({
                                    url: base + 'flight/' + l,
                                    success: function (data) {
                                        if (data != 'fail') {
                                            finish_search(data, l);
                                        }
                                    },
                                    complete: function () {
                                        $('.progressBar').hide();
                                    }
                                });
                            }
                        }
                    });
                }
            }
        });
    }


    //Lấy vé Jetstar
    function get_jetstar($progress) {
        var xJetstar = $.ajax({
            url: base + 'flight/jetstar',
            success: function (data) {
                if (data != 'fail') {
                    finish_search(data, 'jetstar');
                }
            },
            complete: function () {
                $('.progressBar').hide();
            }
        })
    }

    //Lấy danh sách vé Vietnam Airline
    function get_vna($progress) {
        var xVna = $.ajax({
            url: base + 'flight/vna',
            success: function (data) {
                if (data != 'fail') {
                    finish_search(data, 'vna');
                }
            },
            complete: function () {
                $('.progressBar').hide();
            }
        })
    }

    //Lấy danh sách vé Vietjet
    function get_vietjet($progress) {
        var xVietjet = $.ajax({
            url: base + 'flight/vietjet',
            success: function (data) {
                //Nếu có chuyến bay thì mới thực hiện
                if (data != 'fail') {
                    finish_search(data, 'vietjet');
                }
            },
            complete: function () {
                $('.progressBar').hide();
            }
        })
    }



    //Kết thúc Search, lúc này đã hiển thị xong
    function finish_search(data, brand)
    {
        data = $.parseJSON(data);
        if (!$.isEmptyObject(data.outbound)) {
            outbound = outbound.concat(data.outbound);
            outbound.sort(function (a, b) {
                return a.FarePrice.Total.FareCost - b.FarePrice.Total.FareCost;
            });
            list_flights(outbound, brand, 'outbound');
        } else {
            console.log('outbound ' + brand + ' nulled');
        }
        if (!$.isEmptyObject(data.inbound)) {
            inbound = inbound.concat(data.inbound);
            inbound.sort(function (a, b) {
                return a.FarePrice.Total.FareCost - b.FarePrice.Total.FareCost;
            });
            list_flights(inbound, brand, 'inbound');
        } else {
            console.log('inbound ' + brand + ' nulled');
        }
    }



    $('#selected-button').click(function (e) {
        var that = this;
        $.ajax({
            url: base + 'flight/check_selected',
            success: function (data) {

                if (data != 1) {
                    alert('Bạn chưa chọn chiều đi');
                } else {
                    if (roundtype == 'roundtrip') {
                        $.ajax({
                            url: base + 'flight/check_selected/inbound',
                            success: function (data) {
                                if (data != 1) {
                                    alert('Bạn chưa chọn chiều về');
                                } else {
                                    window.location.replace($(that).data('goto'));
                                }
                            }
                        });
                    } else {
                        window.location.replace($(that).data('goto'));
                    }
                }
            }
        });
    });
});
