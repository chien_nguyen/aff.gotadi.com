<?php

class Jetstar {

    //Ngày khởi hành
    private $_ddate;
    //Ngày về
    private $_rdate;
    //Mã sân bay đáp
    private $_acity_code;
    //Mã sân bay khởi hành
    private $_dcity_code;
    //Số người lớn
    private $_adult_count;
    //Trẻ em
    private $_child_count;
    //Trẻ sơ sinh
    private $_inf_count;
    //ONEWAY or ROUNDTRIP
    private $_roundtype;
    //api host
    private $_api_host;
    //CI build
    private $_ci;

    public function __construct() {
        //Lấy cầu hình api host
        $ci = $CI = & get_instance();
        $this->_ci = $ci;
        $this->_api_host = $ci->config->item('jetstarAPI') . 'jetstar.php';
        $this->_ci->load->model('airports_m');
    }

    //Đặt ngày khởi hành
    public function set_ddate($date) {
        $this->_ddate = $date;
    }

    //Đặt ngày trỏ về
    public function set_rdate($date) {
        $this->_rdate = $date;
    }

    //Đặt Mã sân bay đáp
    public function set_acity_code($code) {
        $this->_acity_code = $code;
    }

    //Đặt Mã sân bay khởi hành
    public function set_dcity_code($code) {
        $this->_dcity_code = $code;
    }

    //Đặt số lượng người lớn
    public function set_adult_count($number) {
        $this->_adult_count = $number;
    }

    //Đặt số lượng trẻ em
    public function set_child_count($number) {
        $this->_child_count = $number;
    }

    //Đặt số lượng trẻ sơ sinh
    public function set_inf_count($number) {
        $this->_inf_count = $number;
    }

    //Đặt roundtype
    public function set_roundtype($roundtype) {
        $this->_roundtype = $roundtype;
    }

    //Chạy API
    public function run() {
        $service_url = $this->_build_parameter();
        $curl = curl_init($service_url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, 0);
        curl_setopt($curl, CURLOPT_TIMEOUT, 20); //timeout in seconds
        $curl_response = curl_exec($curl);
        if ($curl_response === false) {
            $info = curl_getinfo($curl);
            curl_close($curl);
        }
        curl_close($curl);
        $decoded = json_decode($curl_response);


        $final['outbound'] = array();
        if (isset($decoded->GetTripAvailabilityResponse->Schedules->ArrayOfJourneyDateMarket[0]->JourneyDateMarket->Journeys->Journey)) {
            $final['outbound'] = $this->get_fare($decoded->GetTripAvailabilityResponse->Schedules->ArrayOfJourneyDateMarket[0]->JourneyDateMarket->Journeys->Journey);
        }


        $final['inbound'] = array();
        if (isset($decoded->GetTripAvailabilityResponse->Schedules->ArrayOfJourneyDateMarket[1]->JourneyDateMarket->Journeys->Journey)) {
            $final['inbound'] = $this->get_fare($decoded->GetTripAvailabilityResponse->Schedules->ArrayOfJourneyDateMarket[1]->JourneyDateMarket->Journeys->Journey);
        }




        return $final;
    }

    //Xây dựng API parameter
    private function _build_parameter() {
        return $this->_api_host . '?acity=' . $this->_acity_code
                . '&dcity=' . $this->_dcity_code
                . '&ddate=' . $this->_ddate
                . '&rdate=' . $this->_rdate
                . '&adult=' . $this->_adult_count
                . '&child=' . $this->_child_count
                . '&inf=' . $this->_inf_count
                . '&roundtype=' . strtoupper($this->_roundtype)
        ;
    }

    private function get_fare($obj) {

        $tmp = array();
        //Trường hợp này tồn tại nhiều hơn 1 chuyến bay
        if (is_array($obj)) {
            //Duyệt qua từng chuyến bay
            foreach ($obj as $flight) {
                //Duyệt qua từng vé
                $segment = $flight->Segments->Segment;
                //Nếu chỉ còn lại 1 loại vé
                if (!is_array($segment)) {

                    $fareArray = (array) $segment->Fares;
                    if (!empty($fareArray)) {
                        foreach ($segment->Fares->Fare as $fare) {
                            $price = new stdClass;
                            foreach ($fare->PaxFares->PaxFare->ServiceCharges->BookingServiceCharge as $value) {
                                if ($value->ChargeType == 'FarePrice') {
                                    $price->FarePrice = (int) $value->Amount;
                                } else if ($value->ChargeType == 'Tax') {
                                    $price->Tax = (int) $value->Amount;
                                } else if ($value->ChargeType == 'ConnectionAdjustmentAmount') {
                                    $price->ConnectionAdjustmentAmount = (int) $value->Amount;
                                }
                            }

                            if (isset($price->Tax) && isset($price->Tax)) {
                                if (($segment->ArrivalStation == $this->_acity_code && $segment->DepartureStation == $this->_dcity_code) ||
                                        ($segment->ArrivalStation == $this->_dcity_code && $segment->DepartureStation == $this->_acity_code)) :
                                    $tmp[] = $this->get_fare_item($segment, $price, $fare, $flight);

                                endif;
                            }
                        }
                    }
                } else {

                    foreach ($segment as $segment_item) {

                        $fareArray = (array) $segment_item->Fares;
                        if (!empty($fareArray)) {
                            foreach ($segment_item->Fares->Fare as $fare) {
                                $BookingServiceCharge = $fare->PaxFares->PaxFare->ServiceCharges->BookingServiceCharge;


                                $price = new stdClass;
                                if (is_array($BookingServiceCharge)) {
                                    foreach ($BookingServiceCharge as $value) {
                                        if ($value->ChargeType == 'FarePrice') {
                                            $price->FarePrice = (int) $value->Amount;
                                        } else if ($value->ChargeType == 'Tax') {
                                            $price->Tax = (int) $value->Amount;
                                        } else if ($value->ChargeType == 'ConnectionAdjustmentAmount') {
                                            $price->ConnectionAdjustmentAmount = (int) $value->Amount;
                                        }
                                    }
                                } else {
                                    if ($BookingServiceCharge->ChargeType == 'FarePrice') {
                                        $price->FarePrice = (int) $BookingServiceCharge->Amount;
                                    } else if ($BookingServiceCharge->ChargeType == 'Tax') {
                                        $price->Tax = (int) $BookingServiceCharge->Amount;
                                    } else if ($BookingServiceCharge->ChargeType == 'ConnectionAdjustmentAmount') {
                                        $price->ConnectionAdjustmentAmount = (int) $BookingServiceCharge->Amount;
                                    }
                                }
                                if (isset($price->Tax) && isset($price->Tax)) {
                                    if (($segment_item->ArrivalStation == $this->_acity_code && $segment_item->DepartureStation == $this->_dcity_code) ||
                                            ($segment_item->ArrivalStation == $this->_dcity_code && $segment_item->DepartureStation == $this->_acity_code)) :
                                        $tmp[] = $this->get_fare_item($segment_item, $price, $fare, $flight);
                                    endif;
                                }
                            }
                        }
                    }
                }
            }
        } else {
            $flight = $obj;
            $segment = $flight->Segments->Segment;
            if (!is_array($segment)) {
                $fareArray = (array) $segment->Fares;
                if (!empty($fareArray)) {
                    foreach ($segment->Fares->Fare as $fare) {
                        $price = new stdClass;
                        foreach ($fare->PaxFares->PaxFare->ServiceCharges->BookingServiceCharge as $value) {
                            if ($value->ChargeType == 'FarePrice') {
                                $price->FarePrice = (int) $value->Amount;
                            } else if ($value->ChargeType == 'Tax') {
                                $price->Tax = (int) $value->Amount;
                            } else if ($value->ChargeType == 'ConnectionAdjustmentAmount') {
                                $price->ConnectionAdjustmentAmount = (int) $value->Amount;
                            }
                        }
                        if (isset($price->Tax) && isset($price->Tax)) {
                            if (($segment->ArrivalStation == $this->_acity_code && $segment->DepartureStation == $this->_dcity_code) ||
                                    ($segment->ArrivalStation == $this->_dcity_code && $segment->DepartureStation == $this->_acity_code)) :
                                $tmp[] = $this->get_fare_item($segment, $price, $fare, $obj);
                            endif;
                        }
                    }
                }
            } else {

                $segment_item = $segment;
                $fareArray = (array) $segment_item->Fares;
                if (!empty($fareArray)) {
                    foreach ($segment_item->Fares->Fare as $fare) {
                        $BookingServiceCharge = $fare->PaxFares->PaxFare->ServiceCharges->BookingServiceCharge;


                        $price = new stdClass;
                        if (is_array($BookingServiceCharge)) {
                            foreach ($BookingServiceCharge as $value) {
                                if ($value->ChargeType == 'FarePrice') {
                                    $price->FarePrice = (int) $value->Amount;
                                } else if ($value->ChargeType == 'Tax') {
                                    $price->Tax = (int) $value->Amount;
                                } else if ($value->ChargeType == 'ConnectionAdjustmentAmount') {
                                    $price->ConnectionAdjustmentAmount = (int) $value->Amount;
                                }
                            }
                        } else {
                            if ($BookingServiceCharge->ChargeType == 'FarePrice') {
                                $price->FarePrice = (int) $BookingServiceCharge->Amount;
                            } else if ($BookingServiceCharge->ChargeType == 'Tax') {
                                $price->Tax = (int) $BookingServiceCharge->Amount;
                            } else if ($BookingServiceCharge->ChargeType == 'ConnectionAdjustmentAmount') {
                                $price->ConnectionAdjustmentAmount = (int) $BookingServiceCharge->Amount;
                            }
                        }
                        if (isset($price->Tax) && isset($price->Tax)) {
                            //Loại bỏ chuyến 2 stop
                            if (($segment_item->ArrivalStation == $this->_acity_code && $segment_item->DepartureStation == $this->_dcity_code) ||
                                    ($segment_item->ArrivalStation == $this->_dcity_code && $segment_item->DepartureStation == $this->_acity_code)) :
                                $tmp[] = $this->get_fare_item($segment_item, $price, $fare, $obj);
                            endif;
                        }
                    }
                }
            }
        }
        return $tmp;
    }

    private function get_fare_item($segment_item, $price, $fare, $obj) {
        
        $markup = ($this->_adult_count + $this->_child_count) * 30000;
        
        return $item = array(
            'id' => md5(microtime() . rand(1, 100)),
            'FlightNumber' => $segment_item->FlightDesignator->FlightNumber,
            'ArrivalStation' => $segment_item->ArrivalStation,
            'DepartureStation' => $segment_item->DepartureStation,
            'acity' => $this->_ci->airports_m->get_info($segment_item->ArrivalStation)->city,
            'dcity' => $this->_ci->airports_m->get_info($segment_item->DepartureStation)->city,
            'aairport' => $this->_ci->airports_m->get_info($segment_item->ArrivalStation)->name,
            'dairport' => $this->_ci->airports_m->get_info($segment_item->DepartureStation)->name,
            'FlightTime' => strtotime($segment_item->STA) - strtotime($segment_item->STD),
            'STD' => date('H:i', strtotime($segment_item->STD)),
            'DDATE' => date('d-m-Y', strtotime($segment_item->STD)),
            'STA' => date('H:i', strtotime($segment_item->STA)),
            'ADATE' => date('d-m-Y', strtotime($segment_item->STA)),
            'dtime' => $segment_item->STD,
            'atime' => $segment_item->STA,
            'FareKey' => $fare->FareSellKey,
            'FareCode' => $fare->CarrierCode,
            'Available' => $fare->AvailableCount,
            'JourneyKey' => $obj->JourneySellKey,
            'ClassOfService' => $fare->ClassOfService,
            'IconName' => $this->_ci->template->dir() . '/img/icon/flight/smBL.gif',
            'Logo' => $this->_ci->template->dir() . '/img/logo/jetstar.png',
            'Brand' => 'Jetstar',
            'B' => 'js',
            'FarePrice' => array(
                'Adult' => array(
                    'FareCost' => round($price->FarePrice * ($this->_adult_count),-3),
                    'Tax' => round($price->Tax * ($this->_adult_count),-3),
                    'Total' => round($price->FarePrice * $this->_adult_count + $price->Tax * $this->_adult_count,-3),
                ),
                'Child' => array(
                    'FareCost' => round($price->FarePrice * ($this->_child_count),-3),
                    'Tax' => round($price->Tax * ($this->_child_count),-3),
                    'Total' => round($price->FarePrice * $this->_child_count + $price->Tax * $this->_child_count,-3),
                ),
                'Total' => array(
                    'FareCost' => round($price->FarePrice * ($this->_adult_count + $this->_child_count),-3),
                    'Tax' => round($this->_adult_count * $price->Tax + ($price->Tax - 40000) * $this->_child_count,-3),
                    'Markup' => round($markup,-3),
                    'TotalFee' => round($this->_adult_count * $price->Tax + ($price->Tax - 40000) * $this->_child_count + $markup,-3),
                    'Total' => round($price->FarePrice * ($this->_adult_count + $this->_child_count) + $this->_adult_count * $price->Tax + ($price->Tax - 40000) * $this->_child_count + $markup,-3),
                ),
            ),
        );
    }

}
