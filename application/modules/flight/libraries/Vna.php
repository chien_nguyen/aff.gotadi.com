<?php

class Vna {

    //Ngày khởi hành
    private $_ddate;
    //Ngày về
    private $_rdate;
    //Mã sân bay đáp
    private $_acity_code;
    //Mã sân bay khởi hành
    private $_dcity_code;
    //Số người lớn
    private $_adult_count;
    //Trẻ em
    private $_child_count;
    //Trẻ sơ sinh
    private $_inf_count;
    //ONEWAY or ROUNDTRIP
    private $_roundtype;
    //api host
    private $_api_host;
    //CI build
    private $_ci;
    private $_commission;

    public function __construct() {
        //Lấy cầu hình api host
        $ci = $CI = & get_instance();
        $this->_ci = $ci;
        $this->_api_host = $ci->config->item('vnaAPI') . 'vna.php';
        $this->_ci->load->model('airports_m');
        $this->_commission = $this->_ci->config->item('commission');
    }

    //Đặt ngày khởi hành
    public function set_ddate($date) {
        $this->_ddate = $date;
    }

    //Đặt ngày trỏ về
    public function set_rdate($date) {
        $this->_rdate = $date;
    }

    //Đặt Mã sân bay đáp
    public function set_acity_code($code) {
        $this->_acity_code = $code;
    }

    //Đặt Mã sân bay khởi hành
    public function set_dcity_code($code) {
        $this->_dcity_code = $code;
    }

    //Đặt số lượng người lớn
    public function set_adult_count($number) {
        $this->_adult_count = $number;
    }

    //Đặt số lượng trẻ em
    public function set_child_count($number) {
        $this->_child_count = $number;
    }

    //Đặt số lượng trẻ sơ sinh
    public function set_inf_count($number) {
        $this->_inf_count = $number;
    }

    //Đặt roundtype
    public function set_roundtype($roundtype) {
        $this->_roundtype = $roundtype;
    }

    //Chạy API
    public function run() {
        $service_url = $this->_build_parameter();
        $curl = curl_init($service_url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, 0);
        curl_setopt($curl, CURLOPT_TIMEOUT, 60); //timeout in seconds
        $curl_response = curl_exec($curl);
        if ($curl_response === false) {
            $info = curl_getinfo($curl);
            curl_close($curl);
        }
        curl_close($curl);
        $decoded = json_decode($curl_response);

        $final['outbound'] = array();
        if (isset($decoded->GetFlightAvailableResult->FlightForDeparture->Flight)) {
            $final['outbound'] = $this->get_fare($decoded->GetFlightAvailableResult->FlightForDeparture->Flight);
        }

        $final['inbound'] = array();
        if (isset($decoded->GetFlightAvailableResult->FlightForReturn->Flight)) {
            $final['inbound'] = $this->get_fare($decoded->GetFlightAvailableResult->FlightForReturn->Flight);
        }

        return $final;
    }

    //Xây dựng API parameter
    private function _build_parameter() {
        return $this->_api_host . '?acity=' . $this->_acity_code
                . '&dcity=' . $this->_dcity_code
                . '&ddate=' . $this->_ddate
                . '&rdate=' . $this->_rdate
                . '&adult=' . $this->_adult_count
                . '&child=' . $this->_child_count
                . '&inf=' . $this->_inf_count
                . '&roundtype=' . strtoupper($this->_roundtype)
        ;
    }

    private function get_fare($obj) {
        $tmp = array();
        $farePrice = 0;
        $exist = 0;

        if (is_array($obj)) {
            foreach ($obj as $flight) {
                if (isset($flight->Classes->FareClass)) {
                    /* Đôi khi số vé còn lại chỉ là 1 class duy nhất thì đây không phải là 1 mảng nữa mà chỉ là 1 object */
                    if (is_array($flight->Classes->FareClass)) {
                        foreach ($flight->Classes->FareClass as $fare) {
                            $item = $this->get_fare_item($flight, $fare);
                        }
                    } else {
                        $fare = $flight->Classes->FareClass;
                        $item = $this->get_fare_item($flight, $fare);
                    }
                    $tmp[] = $item;
                }
            }
        } else {
            $flight = $obj;
            if (isset($flight->Classes->FareClass)) {
                /* Đôi khi số vé còn lại chỉ là 1 class duy nhất thì đây không phải là 1 mảng nữa mà chỉ là 1 object */
                if (is_array($flight->Classes->FareClass)) {
                    foreach ($flight->Classes->FareClass as $fare) {
                        $item = $this->get_fare_item($flight, $fare);
                    }
                } else {
                    $item = $this->get_fare_item($flight, $flight->Classes->FareClass);
                }


                $tmp[] = $item;
            }
        }
        return $tmp;
    }

    private function get_fare_item($flight, $fare) {


        $markup = $this->_adult_count * + 30000 + $this->_child_count * 30000;


        $item = array(
            'id' => md5($flight->FlightNum . strtotime($flight->DepartureDateTime) . $fare->CabinClass . $fare->BookingClass . $flight->DepartureDateTime . rand(1, 100)),
            'FlightNumber' => $flight->FlightNum,
            'ArrivalStation' => substr($flight->Sector, 3, 3),
            'DepartureStation' => substr($flight->Sector, 0, 3),
            'acity' => $this->_ci->airports_m->get_info(substr($flight->Sector, 3, 3))->city,
            'dcity' => $this->_ci->airports_m->get_info(substr($flight->Sector, 0, 3))->city,
            'aairport' => $this->_ci->airports_m->get_info(substr($flight->Sector, 3, 3))->name,
            'dairport' => $this->_ci->airports_m->get_info(substr($flight->Sector, 0, 3))->name,
            'FlightTime' => strtotime($flight->ArrivalDateTime) - strtotime($flight->DepartureDateTime),
            'STD' => date('H:i', strtotime($flight->DepartureDateTime)),
            'DDATE' => date('d-m-Y', strtotime($flight->DepartureDateTime)),
            'STA' => date('H:i', strtotime($flight->ArrivalDateTime)),
            'ADATE' => date('d-m-Y', strtotime($flight->ArrivalDateTime)),
            'FareCode'  => $fare->FareBasic,
            'Available' => ($fare->SeatNumber == 7) ? 9 : $fare->SeatNumber,
            'dtime' => $flight->DepartureDateTime,
            'atime' => $flight->ArrivalDateTime,
            'ClassOfService' => $fare->BookingClass,
            'Brand' => 'Vietnam Airlines',
            'B' => 'vn',
            'IconName' => $this->_ci->template->dir() . '/img/icon/flight/smVN.gif',
            'Logo' => $this->_ci->template->dir() . '/img/logo/vna.png',
            'FarePrice' => array(
                'Adult' => array(
                    'FareCost' => round($fare->Fare,-3),
                    'Tax' => round($fare->Tax,-3),
                    'Total' => round($fare->Fare + $fare->Tax,-3),
                ),
                'Child' => array(
                    'FareChd' => round($fare->FareChd,-3),
                    'TaxChd' => round($fare->TaxChd,-3),
                    'Total' => round($fare->FareChd + $fare->TaxChd,-3)
                ),
                'Inf' => array(
                    'FareInf' => round($fare->FareInf,-3),
                    'TaxInf' => round($fare->TaxInf,-3),
                    'Total' => round($fare->FareInf + $fare->TaxInf,-3)
                ),
                'Total' => array(
                    'FareCost' =>
                    round($this->_adult_count * $fare->Fare + $this->_child_count * $fare->FareChd + $this->_inf_count * $fare->FareInf,-3),
                    'Tax' =>
                    round($this->_adult_count * $fare->Tax + $this->_child_count * $fare->TaxChd + $this->_inf_count * $fare->TaxInf,-3),
                    'Markup' => round($markup,-3),
                    'TotalFee' =>
                    round($this->_adult_count * $fare->Tax + $this->_child_count * $fare->TaxChd + $this->_inf_count * $fare->TaxInf + $markup,-3),
                    'Total' =>
                    round($this->_adult_count * $fare->Fare + $this->_child_count * $fare->FareChd + $this->_inf_count * $fare->FareInf + $this->_adult_count * $fare->Tax + $this->_child_count * $fare->TaxChd + $this->_inf_count * $fare->TaxInf + $markup,-3),
                ),
            ),
        );
        

        return $item;
    }

}
