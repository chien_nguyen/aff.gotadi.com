<?php

class Smartlink_do {

    //private $SECURE_SECRET = "198BE3F2E8C75A53F38C1C4A5B6DBA27";
	private $SECURE_SECRET = "6EED75CD8BC584CF0385DA10D18973FB";
	
    private $vpcURL;
    private $_ci;
    //Tiêu đề
    private $Title;
    //Mã truy cập
    private $vpc_AccessCode;
    //Mã truy cập
    private $vpc_Currency = 'VND';
    //Command
    private $vpc_Command = 'pay';
    //Số tiền
    private $vpc_Amount;
    //Locale
    private $vpc_Locale = 'en';
    //Smartlink Merchant
    private $vpc_Merchant;
    //Thông tin đặt hàng
    private $vpc_OrderInfo;
    //URL trả về (bước xuất vé)
    private $vpc_ReturnURL;
    //Version
    private $vpc_Version = 1;
    //Secure Hash
    private $vpc_SecureHash;
    //Paymend Ref
    private $vpc_MerchTxnRef;

    public function __construct() {
        //Load CI Object
        $ci = $CI = & get_instance();
        $this->_ci = $ci;

        //Đọc file cấu hình config setting
        $this->_ci->load->config('setting');

        //Set vpcURL
        $this->vpcURL = $ci->config->item('smartlink_gateway');

        $this->vpcURL .= '?';

        //Set AccessCode
        $this->vpc_AccessCode = $ci->config->item('smartlink_access_code');

        //Set merchant
        $this->vpc_Merchant = $ci->config->item('smartlink_merchant');
    }

    /**
     * set Title
     * @param type $title
     * @return \Smartlink_do
     */
    public function setTitle($title) {
        $this->Title = $title;
        return $this;
    }

    /**
     * set vpc_AccessCode
     * @param type $code
     * @return \Smartlink_do
     */
    public function setAccessCode($code) {
        $this->vpc_AccessCode = $code;
        return $this;
    }

    /**
     * vpc_Command
     * @param type $command
     * @return \Smartlink_do
     */
    public function setCommand($command) {
        $this->vpc_Command = $command;
        return $this;
    }

    /**
     * set vpc_Currency
     * @param type $currency
     * @return \Smartlink_do
     */
    public function setCurrency($currency) {
        $this->vpc_Currency = $currency;
        return $this;
    }

    /**
     * set vpc_Amount
     * @param type $mount
     * @return \Smartlink_do
     */
    public function setAmount($amount) {
        $this->vpc_Amount = $amount;
        return $this;
    }

    /**
     * set vpc_Locale
     * @param type $locale
     * @return \Smartlink_do
     */
    public function setLocale($locale) {
        $this->vpc_Locale = $locale;
        return $this;
    }

    /**
     * set vpc_OrderInfo
     * @param type $info
     * @return \Smartlink_do
     */
    public function setOrderInfo($info) {
        $this->vpc_OrderInfo = $info;
        return $this;
    }

    /**
     * set vpc_ReturnURL
     * @param type $url
     * @return \Smartlink_do
     */
    public function setReturnURL($url) {
        $this->vpc_ReturnURL = $url;
        return $this;
    }

    /**
     * set vpc_Version
     * @param type $version
     * @return \Smartlink_do
     */
    public function setVersion($version) {
        $this->vpc_Version = $version;
        return $this;
    }

    public function setRef($ref) {
        $this->vpc_MerchTxnRef = $ref;
        return $this;
    }

    public function getGatewayURL() {
        $hash = $this->SECURE_SECRET;
        $url = $this->vpcURL .
                'Title=' . urlencode($this->Title) .
                '&vpc_AccessCode=' . $this->vpc_AccessCode .
                '&vpc_Amount=' . $this->vpc_Amount .
                '&vpc_Command=' . $this->vpc_Command .
                '&vpc_Currency=' . $this->vpc_Currency .
                '&vpc_Locale=' . $this->vpc_Locale .
                '&vpc_MerchTxnRef=' . $this->vpc_MerchTxnRef .
                '&vpc_Merchant=' . $this->vpc_Merchant .
                '&vpc_OrderInfo=' . urlencode($this->vpc_OrderInfo) .
                '&vpc_ReturnURL=' . urlencode($this->vpc_ReturnURL) .
                '&vpc_Version=' . $this->vpc_Version;
                //'&vpc_SecureHash=' . $this->vpc_SecureHash;


        $parts = parse_url($url);
        parse_str($parts['query'], $query);
        
        foreach ($query as $value) {
            $hash = $this->createHash($hash, $value);
        }
        
        $this->vpc_SecureHash = $hash;
        
        $hash = strtoupper(md5($hash));
        $url .= '&vpc_SecureHash='.$hash;
        return $url;
    }

    /**
     * 
     * @param type $hash
     * @param type $item
     * @return type
     */
    private function createHash($hash, $item) {
        if (strlen($item) > 0) {
            $hash .= $item;
        }
        return $hash;
    }

}

//
//// http://localhost/smartlink/vpc_php_merchant_do.php?
//// Title=PHP+Merchant&
//// vpc_AccessCode=ECAFAB&
//// vpc_Amount=100&
//// vpc_Command=pay&
//// vpc_Currency=VND&
//// vpc_Locale=en
//// &vpc_Merchant=SMLTEST&
//// vpc_OrderInfo=VPC+PHP+Merchant+Example&
//// vpc_ReturnURL=http%3A%2F%2Flocalhost%2Fsmartlink%2F&
//// vpc_Version=1&
//// vpc_SecureHash=063470CE47783416A5B140CF7F7434EB
//// Define Constants
//// ----------------
//// This is secret for encoding the MD5 hash
//// This secret will vary from merchant to merchant
//// To not create a secure hash, let SECURE_SECRET be an empty string - ""
//// $SECURE_SECRET = "secure-hash-secret";
//$SECURE_SECRET = "198BE3F2E8C75A53F38C1C4A5B6DBA27";
//
//// add the start of the vpcURL querystring parameters
//$vpcURL = $_POST["virtualPaymentClientURL"] . "?";
//
//// Remove the Virtual Payment Client URL from the parameter hash as we 
//// do not want to send these fields to the Virtual Payment Client.
//unset($_POST["virtualPaymentClientURL"]); 
//unset($_POST["SubButL"]);
//
//// Create the request to the Virtual Payment Client which is a URL encoded GET
//// request. Since we are looping through all the data we may as well sort it in
//// case we want to create a secure hash and add it to the VPC data if the
//// merchant secret has been provided.
//$md5HashData = $SECURE_SECRET;
//ksort ($_POST);
//
//// set a parameter to show the first pair in the URL
//$appendAmp = 0;
//
//foreach($_POST as $key => $value) {
//
//    // create the md5 input and URL leaving out any fields that have no value
//    if (strlen($value) > 0) {
//        
//        // this ensures the first paramter of the URL is preceded by the '?' char
//        if ($appendAmp == 0) {
//            $vpcURL .= urlencode($key) . '=' . urlencode($value);
//            $appendAmp = 1;
//        } else {
//            $vpcURL .= '&' . urlencode($key) . "=" . urlencode($value);
//        }
//        $md5HashData .= $value;
//    }
//}
//
//// Create the secure hash and append it to the Virtual Payment Client Data if
//// the merchant secret has been provided.
//if (strlen($SECURE_SECRET) > 0) {
//    $vpcURL .= "&vpc_SecureHash=" . strtoupper(md5($md5HashData));
//}
//
//// FINISH TRANSACTION - Redirect the customers using the Digital Order
//// ===================================================================
//header("Location: ".$vpcURL);

