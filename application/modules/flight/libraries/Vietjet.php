<?php

class Vietjet {

    //Ngày khởi hành
    private $_ddate;
    //Ngày về
    private $_rdate;
    //Mã sân bay đáp
    private $_acity_code;
    //Mã sân bay khởi hành
    private $_dcity_code;
    //Số người lớn
    private $_adult_count;
    //Trẻ em
    private $_child_count;
    //Trẻ sơ sinh
    private $_inf_count;
    //ONEWAY or ROUNDTRIP
    private $_roundtype;
    //api host
    private $_api_host;
    //CI build
    private $_ci;
    private $_curl_response;

    public function __construct() {
        //Lấy cầu hình api host
        $ci = $CI = & get_instance();
        $this->_ci = $ci;
        $this->_api_host = $ci->config->item('vietjetAPI') . 'vietjet.php';
        $this->_ci->load->model('airports_m');
    }

    //Đặt ngày khởi hành
    public function set_ddate($date) {
        $this->_ddate = $date;
    }

    //Đặt ngày trỏ về
    public function set_rdate($date) {
        $this->_rdate = $date;
    }

    //Đặt Mã sân bay đáp
    public function set_acity_code($code) {
        $this->_acity_code = $code;
    }

    //Đặt Mã sân bay khởi hành
    public function set_dcity_code($code) {
        $this->_dcity_code = $code;
    }

    //Đặt số lượng người lớn
    public function set_adult_count($number) {
        $this->_adult_count = $number;
    }

    //Đặt số lượng trẻ em
    public function set_child_count($number) {
        $this->_child_count = $number;
    }

    //Đặt số lượng trẻ sơ sinh
    public function set_inf_count($number) {
        $this->_inf_count = $number;
    }

    //Đặt roundtype
    public function set_roundtype($roundtype) {
        $this->_roundtype = $roundtype;
    }

    //Chạy API
    public function run() {
        $service_url = $this->_build_parameter();
        $curl = curl_init($service_url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, 0);
        curl_setopt($curl, CURLOPT_TIMEOUT, 60); //timeout in seconds
        $curl_response = curl_exec($curl);
        if ($curl_response === false) {
            $info = curl_getinfo($curl);
            curl_close($curl);
        }
        curl_close($curl);
        $decoded = json_decode($curl_response);

        $this->_curl_response = $decoded;

        $final['outbound'] = array();
        if (isset($decoded->GetTravelOptionsResult->TravelOptions->OutboundOptions->Option)) {
            $final['outbound'] = $this->get_fare($decoded->GetTravelOptionsResult->TravelOptions->OutboundOptions->Option);
        }

        $final['inbound'] = array();
        if (isset($decoded->GetTravelOptionsResult->TravelOptions->InboundOptions->Option)) {
            $final['inbound'] = $this->get_fare($decoded->GetTravelOptionsResult->TravelOptions->InboundOptions->Option);
        }

        return $final;
    }

    public function get_outbound() {
        $outbound = array();
        if (isset($decoded->GetTravelOptionsResult->TravelOptions->OutboundOptions->Option)) {
            $outbound = $this->get_fare($decoded->GetTravelOptionsResult->TravelOptions->OutboundOptions->Option);
        }
        return $outbound;
    }

    //Xây dựng API parameter
    private function _build_parameter() {
        return $this->_api_host . '?acity=' . $this->_acity_code
                . '&dcity=' . $this->_dcity_code
                . '&ddate=' . $this->_ddate
                . '&rdate=' . $this->_rdate
                . '&adult=' . $this->_adult_count
                . '&child=' . $this->_child_count
                . '&inf=' . $this->_inf_count
                . '&roundtype=' . strtoupper($this->_roundtype)
        ;
    }

    private function get_fare($object) {
        $tmp = array();

        //Duyệt qua tất cả chuyến bay
        foreach ($object as $f) {

            //Nếu tồn tại Legs
            if (isset($f->Legs) && isset($f->Legs->LegOption->SegmentOptions->SegmentOption->Flight)) {
                
                $flight = $f->Legs->LegOption->SegmentOptions->SegmentOption->Flight;
                $Fare = $f->Legs->LegOption->FareOptions;
                $Surcharge = $f->Legs->LegOption->Surcharges->Surcharge;
                if ($flight != NULL) {
                    if (is_array($Fare->Adultfares->FareOption)) {
                        foreach ($Fare->Adultfares->FareOption as $fare) {
                            $tmp[] = $this->get_fare_item($flight, $fare, $Surcharge);
                        }
                    } else {
                        $fare = $Fare->Adultfares->FareOption;
                        $tmp[] = $this->get_fare_item($flight, $fare, $Surcharge);
                    }
                }
            }
        }
        return $tmp;
    }

    private function get_fare_item($flight, $fare, $Surcharge) {

        //Phụ phí

        $surc_total = 0;
        foreach ($Surcharge as $surc) {
            if ($surc->ApplyToAdult == true) {
                $surc_total += $surc->Total * $this->_adult_count;
            }
            if ($surc->ApplyToChild == true) {
                $surc_total += $surc->Total * $this->_child_count;
            }
            if ($surc->ApplyToInfant == true) {
                $surc_total += $surc->Total * $this->_inf_count;
            }
        }

        $markup = ($this->_adult_count + $this->_child_count) * 30000;

        return $item = array(
            'id' => md5(microtime() . rand(1, 100)),
            'FlightNumber' => $flight->Number,
            'ArrivalStation' => $flight->ArrivalAirport->Code,
            'DepartureStation' => $flight->DepartureAirport->Code,
            'acity' => $this->_ci->airports_m->get_info($flight->ArrivalAirport->Code)->city,
            'dcity' => $this->_ci->airports_m->get_info($flight->DepartureAirport->Code)->city,
            'aairport' => $this->_ci->airports_m->get_info($flight->ArrivalAirport->Code)->name,
            'dairport' => $this->_ci->airports_m->get_info($flight->DepartureAirport->Code)->name,
            'FlightTime' => strtotime($flight->ETALocal) - strtotime($flight->ETDLocal),
            'STD' => date('H:i', strtotime($flight->ETDLocal)),
            'DDATE' => date('d-m-Y', strtotime($flight->ETDLocal)),
            'STA' => date('H:i', strtotime($flight->ETALocal)),
            'ADATE' => date('d-m-Y', strtotime($flight->ETALocal)),
            'dtime' => $flight->ETDLocal,
            'Available' => $fare->SeatsAvailable,
            'atime' => $flight->ETALocal,
            'ClassOfService' => isset($fare->FareCategory) ? $fare->FareCategory : "",
            'FareCode' => $fare->FareClass,
            'Brand' => 'Vietjet',
            'B' => 'vj',
            'IconName' => $this->_ci->template->dir() . '/img/icon/flight/smVJ.gif',
            'Logo' => $this->_ci->template->dir() . '/img/logo/vietjet.png',
            'FarePrice' => array(
                'Adult' => array(
                    'FareCost' => round($fare->FareCost * $this->_adult_count,-3),
                    'Tax' => round($fare->FareTaxes * $this->_adult_count,-3),
                    'Total' => round($fare->FareCost * $this->_adult_count + $fare->FareTaxes * $this->_adult_count,-3),
                ),
                'Child' => array(
                    'FareCost' => round($fare->FareCost * $this->_child_count,-3),
                    'Tax' => round($fare->FareTaxes * $this->_child_count,-3),
                    'Total' => round($fare->FareCost * $this->_child_count + $fare->FareTaxes * $this->_child_count,-3),
                ),
                'Total' => array(
                    'FareCost' => round($fare->FareCost * ($this->_adult_count + $this->_child_count),-3),
                    'Tax' => round($fare->FareTaxes * ($this->_adult_count + $this->_child_count) + $surc_total,-3),
                    'Markup' => round($markup,-3),
                    'TotalFee' => round($markup + $fare->FareTaxes * ($this->_adult_count + $this->_child_count) + $surc_total,-3),
                    'Total' => round($fare->FareTaxes * ($this->_adult_count + $this->_child_count) + $surc_total + $fare->FareCost * ($this->_adult_count + $this->_child_count) + $markup,-3),
                ),
            ),
        );
    }

}
