<?php

class Smartlink_dr {

    //Secure Hash
    //private $SECURE_SECRET = "198BE3F2E8C75A53F38C1C4A5B6DBA27";
	private $SECURE_SECRET = "6EED75CD8BC584CF0385DA10D18973FB";
    private $vpcURL;
    private $_ci;

    public function __construct() {
        //Load CI Object
        $ci = $CI = & get_instance();
        $this->_ci = $ci;

        //Đọc file cấu hình config setting
        $this->_ci->load->config('setting');

        //Set vpcURL
        $this->vpcURL = $ci->config->item('smartlink_gateway');

        $this->vpcURL .= '?';

        //Set AccessCode
        $this->vpc_AccessCode = $ci->config->item('smartlink_access_code');

        //Set merchant
        $this->vpc_Merchant = $ci->config->item('smartlink_merchant');
    }

    public function check() {
        // get and remove the vpc_TxnResponseCode code from the response fields as we
        // do not want to include this field in the hash calculation
        $vpc_Txn_Secure_Hash = $_GET["vpc_SecureHash"];
        unset($_GET["vpc_SecureHash"]);

        // set a flag to indicate if hash has been validated
        $errorExists = false;

        if (strlen($this->SECURE_SECRET) > 0 && $_GET["vpc_TxnResponseCode"] != "No Value Returned") {

            $md5HashData = $this->SECURE_SECRET;

            // sort all the incoming vpc response fields and leave out any with no value
            foreach ($_GET as $key => $value) {
                if ($key != "vpc_SecureHash" or strlen($value) > 0) {
                    $md5HashData .= $value;
                }
            }

            // Validate the Secure Hash (remember MD5 hashes are not case sensitive)
            // This is just one way of displaying the result of checking the hash.
            // In production, you would work out your own way of presenting the result.
            // The hash check is all about detecting if the data has changed in transit.
            if (strtoupper($vpc_Txn_Secure_Hash) == strtoupper(md5($md5HashData))) {
                // Secure Hash validation succeeded, add a data field to be displayed
                // later.
                $hashValidated = "<FONT color='#00AA00'><strong>CORRECT</strong></FONT>";
            } else {
                // Secure Hash validation failed, add a data field to be displayed
                // later.
                $hashValidated = "<FONT color='#FF0066'><strong>INVALID HASH</strong></FONT>";
                $errorExists = true;
            }
        } else {
            // Secure Hash was not validated, add a data field to be displayed later.
            $hashValidated = "<FONT color='orange'><strong>Not Calculated - No 'SECURE_SECRET' present.</strong></FONT>";
        }

        $txnResponseCode = $this->null2unknown($_GET["vpc_TxnResponseCode"]);

        // *******************
        // END OF MAIN PROGRAM
        // *******************
        // FINISH TRANSACTION - Process the VPC Response Data
        // =====================================================
        // For the purposes of demonstration, we simply display the Result fields on a
        // web page.
        // Show 'Error' in title if an error condition
        $errorTxt = "";

        // Show this page as an error page if vpc_TxnResponseCode equals '0'
        if ($txnResponseCode != "0" || $txnResponseCode == "No Value Returned" || $errorExists) {
            return false;
        } else {
            $txnResponseCode;
            return $txnResponseCode;
        }
    }

    private function getResponseDescription($responseCode) {

        switch ($responseCode) {
            case "0" : $result = "Giao dich thanh cong";
                break;
            case "1" : $result = "Ngan hang tu choi thanh toan: the/tai khoan bi khoa";
                break;
            case "2" : $result = "Loi so 2";
                break;
            case "3" : $result = "The het han";
                break;
            case "4" : $result = "Qua so lan giao dich cho phep. (Sai OTP, qua han muc trong ngay)";
                break;
            case "5" : $result = "Khong co tra loi tu Ngan hang";
                break;
            case "6" : $result = "Loi giao tiep voi Ngan hang";
                break;
            case "7" : $result = "Tai khoan khong du tien";
                break;
            case "8" : $result = "Loi du lieu truyen";
                break;
            case "9" : $result = "Kieu giao dich khong duoc ho tro";
                break;
            default : $result = "Loi khong xac dinh";
        }
        return $result;
    }

    private function null2unknown($data) {
        if ($data == "") {
            return "No Value Returned";
        } else {
            return $data;
        }
    }

}
