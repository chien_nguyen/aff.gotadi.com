<?php

class Visa_do {

    private $SECURE_SECRET = "B575ED17E000D6E2BD8634FD0E6B042D";
    private $vpcURL;
    private $_ci;
    //Tiêu đề
    private $Title;
    //Mã truy cập
    private $vpc_AccessCode;
    //Command
    private $vpc_Command = 'pay';
    //Số tiền
    private $vpc_Amount;
    //Locale
    private $vpc_Locale = 'VN';
    //Smartlink Merchant
    private $vpc_Merchant;
    //Thông tin đặt hàng
    private $vpc_OrderInfo;
    //URL trả về (bước xuất vé)
    private $vpc_ReturnURL;
    //Version
    private $vpc_Version = 1;
    //Secure Hash
    private $vpc_SecureHash;
    //Paymend Ref
    private $vpc_MerchTxnRef;
    private $vpc_TicketNo;
    private $vpc_TxSourceSubType;
    private $paymentParams = array();

    public function __construct() {
        //Load CI Object
        $ci = $CI = & get_instance();
        $this->_ci = $ci;

        //Đọc file cấu hình config setting
        $ci->load->config('setting');


        //Set vpcURL
        $this->vpcURL = $ci->config->item('visaGateway');

        $this->vpcURL .= '?';

        $this->paymentParams['vpc_AccessCode'] = $ci->config->item('visaAccessCode');
        $this->paymentParams['vpc_Merchant'] = $ci->config->item('visaMerchant');
        $this->paymentParams['vpc_MerchTxnRef'] = $ci->config->item('visaMerchTxnRef');
        $this->paymentParams['vpc_Command'] = 'pay';
        $this->paymentParams['vpc_Locale'] = 'VN';
        $this->paymentParams['vpc_Version'] = '1.0';
        $this->paymentParams['vpc_TicketNo'] = '';
        $this->paymentParams['vpc_TxSourceSubType'] = '';
    }

    /**
     * set Title
     * @param type $title
     * @return \Smartlink_do
     */
    public function setTitle($title) {
        $this->paymentParams['Title'] = $title;
        return $this;
    }

    /**
     * set vpc_AccessCode
     * @param type $code
     * @return \Smartlink_do
     */
    public function setAccessCode($code) {
        $this->paymentParams['vpc_AccessCode'] = $code;
        return $this;
    }

    /**
     * set vpc_Amount
     * @param type $mount
     * @return \Smartlink_do
     */
    public function setAmount($amount) {
        $this->paymentParams['vpc_Amount'] = $amount;
        return $this;
    }

    /**
     * vpc_Command
     * @param type $command
     * @return \Smartlink_do
     */
    public function setCommand($command) {
        $this->paymentParams['vpc_Command'] = $command;
        return $this;
    }

    /**
     * set vpc_Locale
     * @param type $locale
     * @return \Smartlink_do
     */
    public function setLocale($locale) {
        $this->paymentParams['vpc_Locale'] = $locale;
        return $this;
    }

    /**
     * set vpc_OrderInfo
     * @param type $info
     * @return \Smartlink_do
     */
    public function setOrderInfo($info) {
        $this->paymentParams['vpc_OrderInfo'] = $info;
        return $this;
    }

    /**
     * set vpc_ReturnURL
     * @param type $url
     * @return \Smartlink_do
     */
    public function setReturnURL($url) {
        $this->paymentParams['vpc_ReturnURL'] = $url;
        return $this;
    }

    /**
     * set vpc_Version
     * @param type $version
     * @return \Smartlink_do
     */
    public function setVersion($version) {
        $this->paymentParams['vpc_Version'] = $version;
        return $this;
    }

    
    /**
     * 
     * @param type $ref
     * @return \Visa_do
     */
    public function setRef($ref) {
        $this->paymentParams['vpc_MerchTxnRef'] = $ref;
        return $this;
    }

    
    /**
     * 
     * @return string
     */
    public function getGatewayURL() {
        $appendAmp = 0;
        $vpcURL = $this->_ci->config->item('visaGateway') . '?';
        $md5HashData = $this->SECURE_SECRET;
        ksort($this->paymentParams);
        
        foreach ($this->paymentParams as $key => $value) {

            // create the md5 input and URL leaving out any fields that have no value
            if (strlen($value) > 0) {

                // this ensures the first paramter of the URL is preceded by the '?' char
                if ($appendAmp == 0) {
                    $vpcURL .= urlencode($key) . '=' . urlencode($value);
                    $appendAmp = 1;
                } else {
                    $vpcURL .= '&' . urlencode($key) . "=" . urlencode($value);
                }
                $md5HashData .= $value;
            }
        }

        if (strlen($this->SECURE_SECRET) > 0) {
            $vpcURL .= "&vpc_SecureHash=" . strtoupper(md5($md5HashData));
        }
        
        return $vpcURL;
    }

}



