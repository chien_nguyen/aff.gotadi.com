<?php
defined('BASEPATH') or exit('Không được quyền truy cập');

class Admin extends Admin_Controller
{
    public function __construct() {
        parent::__construct();
    }
    
    public function index()
    {
        $this->template->set_layout('table_layout')->build('admin/index');
    }
    
    
    
}