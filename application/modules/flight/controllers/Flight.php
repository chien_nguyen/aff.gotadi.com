<?php

defined('BASEPATH') or exit('Không được quyền truy cập');

class Flight extends MX_Controller {

    private $_api_host;
    private $_all_flights = array();
    private $_soap_options = array(
        'soap_version' => SOAP_1_1,
        'exceptions' => true,
        'encoding' => 'UTF-8',
        'content-type' => 'text/xml'
    );

    //Constructor    
    public function __construct() {
        parent::__construct();
        //Đặt giao diện
        $this->template->set_theme('thuyta');

        //Đặt title mặc định
        $this->template->title('Đặt v&#233; m&#225;y bay v&#224; kh&#225;ch sạn to&#224;n thế giới | Gotadi.com')
                ->set_layout('default');

        //Load model sân bay
        $this->load->model('airports_m');

        //Load cache model
        $this->load->model('cache_m');

        //Nếu tồn tại Affiliate
        if (isset($_GET['aff'])) {
            $this->session->set_userdata('aff', $_GET['aff']);
        }

        //Đặt thẻ bọc lỗi cho validate dữ liệu người dùng
        $this->form_validation->set_error_delimiters('<div class="text-danger">', '</div>');

        //Load cấu hình
        $this->load->config('setting');

        //Lấy Desktop link
        $this->template->set('desktop_link', $this->config->item('desktop_link'));
    }

    public function index() {

        if (isset($_SESSION['finished'])) {
            $this->session->unset_userdata('finished');
        }

        $this->form_validation->set_rules(
                array(
                    array(
                        'field' => 'dcity_code',
                        'label' => 'Nơi đi',
                        'rules' => 'required'
                    ),
                    array(
                        'field' => 'acity_code',
                        'label' => 'Nơi đến',
                        'rules' => 'required|callback__check_acity'
                    ),
                    array(
                        'field' => 'rdate_submit',
                        'label' => 'Ngày về',
                        'rules' => 'required|callback__check_rdate'
                    ),
                    array(
                        'field' => 'adult',
                        'label' => 'Tổng số khách',
                        'rules' => 'required|callback__check_adult'
                    ),
                    array(
                        'field' => 'ddate',
                        'label' => 'Ngày đi',
                        'rules' => 'required'
                    ),
                )
        );


        $airports = $this->airports_m->get_all();
        if ($this->form_validation->run($this)) {

            $array = $_POST;
            $array['acity'] = $this->airports_m->get_city($_POST['acity_code']);
            $array['dcity'] = $this->airports_m->get_city($_POST['dcity_code']);
            $this->session->set_userdata('search_data', $array);

            //Unset các chuyến bay đã chọn trước đó
            $this->unset_selected();

            redirect('flight/search_result');
        } else {
            $this->template->set('airports', $airports)
                    ->build('flight/flight_home');
        }
    }

    public function _check_rdate() {
        $a = strtotime($_POST['ddate_submit']);
        $b = strtotime($_POST['rdate_submit']);
        $roundtype = $_POST['roundtype'];
        if ($roundtype == 'roundtrip') {
            if ($a > $b) {
                $this->form_validation->set_message('_check_rdate', 'Ngày về không được nhỏ hơn ngày đi');
                return FALSE;
            } else {
                return true;
            }
        }
        return true;
    }

    public function _check_acity() {
        $a = $_POST['acity_code'];
        $b = $_POST['dcity_code'];

        if ($a == $b) {

            $this->form_validation->set_message('_check_acity', 'Nơi đi và nơi đến không được giống nhau');
            return FALSE;
        } else {
            return true;
        }
    }

    private function unset_selected() {
        if (isset($_SESSION['selected_outbound'])) {
            $this->session->unset_userdata('selected_outbound');
        }
        if (isset($_SESSION['selected_inbound'])) {
            $this->session->unset_userdata('selected_inbound');
        }
        if (isset($_SESSION['totalFare'])) {
            $this->session->unset_userdata('totalFare');
        }
        if (isset($_SESSION['passenger'])) {
            $this->session->unset_userdata('passenger');
        }
    }

    
    /**************************************************************************
     * 
     **************************************************************************/
    public function search_result() {
        if (!isset($_SESSION['search_data']))
            redirect();
        $this->_all_flights['inbound'] = array();
        $this->_all_flights['outbound'] = array();
        $this->session->set_userdata('result', $this->_all_flights);

        $this->unset_selected();

        $this->template->set('search_data', $_SESSION['search_data'])
                ->title('Tìm vé máy bay')
                ->append_js('search.js')
                ->build('flight/search_result');
    }

    /**
     * Tại đây khách sẽ nhập thông tin hành khách, lựa chọn phương thức thanh toán
     */
    public function passengers() {
        if (!isset($_SESSION['selected_inbound']) & !isset($_SESSION['selected_outbound'])) {
            redirect();
        }


        for ($i = 1; $i <= $_SESSION['search_data']['adult']; $i++) {
            $this->form_validation->set_rules('adult_firstname[' . $i . ']', 'Họ người lớn thứ ' . $i, 'required|trim');
            $this->form_validation->set_rules('adult_lastname[' . $i . ']', 'Tên đệm và tên người lớn thứ ' . $i, 'required|trim');
            $this->form_validation->set_rules('adult_title[' . $i . ']', 'Giới tính người lớn thứ ' . $i, 'required|trim');
            //$this->form_validation->set_rules('adult_luggage[' . $i . ']', 'Hành lý ký gửi người thứ ' . $i, 'required|trim');
        }
        for ($i = 1; $i <= $_SESSION['search_data']['child']; $i++) {
            $this->form_validation->set_rules('child_firstname[' . $i . ']', 'Họ trẻ em thứ ' . $i, 'required|trim');
            $this->form_validation->set_rules('child_lastname[' . $i . ']', 'Tên đệm và tên trẻ em thứ ' . $i, 'required|trim');
            $this->form_validation->set_rules('child_title[' . $i . ']', 'Giới tính trẻ em thứ ' . $i, 'required|trim');
            $this->form_validation->set_rules('child_birthday[' . $i . ']', 'Ngày sinh trẻ em thứ ' . $i, 'required|trim');
        }
        for ($i = 1; $i <= $_SESSION['search_data']['inf']; $i++) {
            $this->form_validation->set_rules('inf_firstname[' . $i . ']', 'Họ trẻ sơ sinh thứ ' . $i, 'required|trim');
            $this->form_validation->set_rules('inf_lastname[' . $i . ']', 'Tên đệm và tên trẻ sơ sinh thứ ' . $i, 'required|trim');
            $this->form_validation->set_rules('inf_title[' . $i . ']', 'Giới tính trẻ sơ sinh thứ ' . $i, 'required|trim');
            $this->form_validation->set_rules('inf_birthday[' . $i . ']', 'Ngày sinh Trẻ sơ sinh thứ ' . $i, 'required|trim');
        }

        $this->form_validation->set_rules('contact_name', 'Tên người liên hệ', 'required|trim|max_length[50]|callback__check_contact_name');
        $this->form_validation->set_rules('contact_title', 'Giới tính người liên hệ', 'required|trim');
        $this->form_validation->set_rules('contact_phone', 'Số điện thoại người liên hệ', 'required|trim|numeric|min_length[9]|max_length[11]');
        $this->form_validation->set_rules('contact_address', 'Địa chỉ người liên hệ', 'required|trim');
        $this->form_validation->set_rules('contact_email', 'Địa chỉ email', 'required|trim|valid_email');
        $this->form_validation->set_rules('policy', 'Điểu khoản sử dụng', 'callback__check_policy');
        $this->form_validation->set_rules('payment_method', 'Phương thức thanh toán', 'callback__check_payment_method');



        $user = array();
        if (!empty($_POST)) {

            for ($i = 1; $i <= $_SESSION['search_data']['adult']; $i++) {
                $user['adult_firstname'][$i] = $_POST['adult_firstname'][$i];
                $user['adult_lastname'][$i] = $_POST['adult_lastname'][$i];
                // $user['adult_luggage'][$i] = $_POST['adult_luggage'][$i];
                $user['adult_title'][$i] = $_POST['adult_title'][$i];
            }
            for ($i = 1; $i <= $_SESSION['search_data']['child']; $i++) {
                $user['child_firstname'][$i] = $_POST['child_firstname'][$i];
                $user['child_lastname'][$i] = $_POST['child_lastname'][$i];
                $user['child_title'][$i] = $_POST['child_title'][$i];
                $user['child_birthday'][$i] = $_POST['child_birthday'][$i];
            }
            for ($i = 1; $i <= $_SESSION['search_data']['inf']; $i++) {
                $user['inf_firstname'][$i] = $_POST['inf_firstname'][$i];
                $user['inf_lastname'][$i] = $_POST['inf_lastname'][$i];
                $user['inf_title'][$i] = $_POST['inf_title'][$i];
                $user['inf_birthday'][$i] = $_POST['inf_birthday'][$i];
            }
            $user['contact_name'] = $_POST['contact_name'];
            $user['contact_title'] = $_POST['contact_title'];
            $user['contact_phone'] = $_POST['contact_phone'];
            $user['contact_phone_2'] = $_POST['contact_phone_2'];
            $user['contact_address'] = $_POST['contact_address'];
            $user['contact_email'] = $_POST['contact_email'];
        } else {

            if (!isset($_SESSION['passenger'])) {
                for ($i = 1; $i <= $_SESSION['search_data']['adult']; $i++) {
                    $user['adult_firstname'][$i] = '';
                    //$user['adult_luggage'][$i] = '';
                    $user['adult_lastname'][$i] = '';
                    $user['adult_title'][$i] = 'Ông';
                }
                for ($i = 1; $i <= $_SESSION['search_data']['child']; $i++) {
                    $user['child_firstname'][$i] = '';
                    $user['child_lastname'][$i] = '';
                    $user['child_title'][$i] = 'Bé trai';
                    $user['child_birthday'][$i] = '';
                }
                for ($i = 1; $i <= $_SESSION['search_data']['inf']; $i++) {
                    $user['inf_firstname'][$i] = '';
                    $user['inf_lastname'][$i] = '';
                    $user['inf_title'][$i] = 'Bé trai';
                    $user['inf_birthday'][$i] = '';
                }
                $user['contact_name'] = '';
                $user['contact_title'] = 'Ông';
                $user['contact_phone'] = '';
                $user['contact_phone_2'] = '';
                $user['contact_address'] = '';
                $user['contact_email'] = '';
            } else {
                for ($i = 1; $i <= $_SESSION['search_data']['adult']; $i++) {
                    $user['adult_firstname'][$i] = $_SESSION['passenger']['adult_firstname'][$i];
                    $user['adult_lastname'][$i] = $_SESSION['passenger']['adult_lastname'][$i];
                    //$user['adult_luggage'][$i] = $_SESSION['passenger']['adult_luggage'][$i];
                    $user['adult_title'][$i] = $_SESSION['passenger']['adult_title'][$i];
                }
                for ($i = 1; $i <= $_SESSION['search_data']['child']; $i++) {
                    $user['child_firstname'][$i] = $_SESSION['passenger']['child_firstname'][$i];
                    $user['child_lastname'][$i] = $_SESSION['passenger']['child_lastname'][$i];
                    $user['child_title'][$i] = $_SESSION['passenger']['child_title'][$i];
                    $user['child_birthday'][$i] = $_SESSION['passenger']['child_birthday'][$i];
                }
                for ($i = 1; $i <= $_SESSION['search_data']['inf']; $i++) {
                    $user['inf_title'][$i] = $_SESSION['passenger']['inf_title'][$i];
                    $user['inf_firstname'][$i] = $_SESSION['passenger']['inf_firstname'][$i];
                    $user['inf_lastname'][$i] = $_SESSION['passenger']['inf_lastname'][$i];
                    $user['inf_birthday'][$i] = $_SESSION['passenger']['inf_birthday'][$i];
                }
                $user['contact_name'] = $_SESSION['passenger']['contact_name'];
                $user['contact_title'] = $_SESSION['passenger']['contact_title'];
                $user['contact_phone'] = $_SESSION['passenger']['contact_phone'];
                $user['contact_phone_2'] = $_SESSION['passenger']['contact_phone_2'];
                $user['contact_address'] = $_SESSION['passenger']['contact_address'];
                $user['contact_email'] = $_SESSION['passenger']['contact_email'];
            }
        }


        if ($this->form_validation->run($this) != FALSE) {
            $this->session->set_userdata('passenger', $_POST);
            $this->session->set_userdata('paymentRef', date('YmdHis', time()));
            redirect('flight/confirm');
        } else {
            $this->template->set('search_data', $_SESSION['search_data'])
                    ->set('user', $user)
                    ->build('flight/passengers');
        }
    }

    /**
     * chuyển khách hàng đến trang thanh toán tại Smartlink
     */
    private function smartlink_ib($paymentRef = false) {
        $this->load->library('smartlink_do');

        

        //Tổng số tiền khách phải trả
        $amount = $_SESSION['selected_outbound']->FarePrice->Total->Total;

        if ($_SESSION['search_data']['roundtype'] == 'roundtrip') {
            $amount += $_SESSION['selected_inbound']->FarePrice->Total->Total;
        }


        $paymentFee = 0;
        if ($_SESSION['passenger']['payment_method'] == 'internet_banking') {
            $paymentFee = $this->config->item('atmFee');
        } else if ($_SESSION['passenger']['payment_method'] == 'visa_master') {
            $paymentFee = $this->config->item('visaFee');
        }

        $this->smartlink_do->setAmount(round(($_SESSION['totalFare'] + $_SESSION['totalFare'] * $paymentFee), -3) * 100);

        //Tiêu đề đặt hàng
        $this->smartlink_do->setTitle('AirBooking');

        $bookingRef = date('YmdHis', time());
        //Thông tin đặt hàng
        $this->smartlink_do->setOrderInfo($paymentRef);
        $this->smartlink_do->setRef($paymentRef);
        
        //Đường dẫn trả lại
        $this->smartlink_do->setReturnURL(base_url('flight/smartlink_return'));

        //Lấy đường dẫn
        $url = $this->smartlink_do->getGatewayURL();

        //Chuyển khách đến smartlink
        $this->session->set_userdata('paymentURL', $url);
        $this->session->set_userdata('PaymentRef', $bookingRef);
        redirect($url);
    }

    /**
     * 
     * @param type $post
     * @return type
     */
    private function get_form_validtion_error($post) {
        $array = array();
        foreach ($post as $key => $value) {
            $array[$key] = form_error($key);
        }
        return $array;
    }

    /**
     * Bắt buộc check vào điều khoản sử dụng để tiếp tục
     * @return boolean
     */
    public function _check_policy() {
        if ($this->input->post('policy') != "") {
            return TRUE;
        } else {
            $this->form_validation->set_message('_check_policy', 'Bạn chưa đồng ý với điều khoản sử dụng');
            return FALSE;
        }
    }

    /**
     * Người dùng xác nhận thông tin tại bước này, sau khi họ đã chọn phương thức thanh toán
     */
    public function confirm() {
			
        $this->load->model('booking_m');
	 
        if (!isset($_SESSION['passenger']))
            redirect();

        if (isset($_SESSION['finished'])) {
            redirect();
			
        }
	
        $this->form_validation->set_rules('submit', 'submit', 'required');
        if ($this->form_validation->run($this)) {

		
            //Booking giữ chổ, chuyển khoản offline
            if ($_SESSION['passenger']['payment_method'] == 'bank_transfer') {
                $result = $this->booking_m->create();
				
                //Trường hợp đúng
                if ($result != false) {
                    $this->session->set_userdata('bookingResult', $result->CreateBookingResult);
                    //$this->email($result->CreateBookingResult);
                    redirect('flight/finish');
                }
                //Trường hợp không thành công
                else {
                    $this->session->set_flashdata('error', 'Vé chọn đã hết ghế');
                    redirect('flight/unknown_error');
                }
            } else if ($_SESSION['passenger']['payment_method'] == 'internet_banking') {
                //Booking trực tiếp xuất vé
                //Booking tạm
                $result = $this->booking_m->create_tentative();
                if($result != false){
                    $this->session->set_userdata('tentative', $result->CreateBookingResult);
                    //Chuyển đến trang thanh toán
                    
                    $this->smartlink_ib($result->CreateBookingResult->PNR);
                }else{
                    $this->session->set_flashdata('error', 'Create Tentative Error !');
                    redirect('flight/unknown_error');
                }
            } else if ($_SESSION['passenger']['payment_method'] == 'visa_master') {
                //chưa thực hiện được cho visa, master
            } else {
                //Vì một lý do nào đó mà không tồn tại phương thức thanh toán
                //Chúng ta không xử lý trường hợp này
                $this->session->set_flashdata('error', 'Unknown Payment Method');
				redirect('flight/unknown_error');
            }
        } else {
            $this->template->set('search_data', $_SESSION['search_data'])
                    ->set('passenger', $_SESSION['passenger'])
                    ->build('flight/confirm');
        }
    }

    public function unknown_error() {
        $this->template->build('flight/unknown_error');
    }

    public function error() {
        $this->template->build('flight/error');
    }

    /**
     * Hàm này nhận dữ liệu từ Smartlink trở về
     */
    public function smartlink_return() {
        $this->load->library('smartlink_dr');
        $checkReturn = $this->smartlink_dr->check();
        if ($checkReturn === FALSE) {
            redirect('flight/unknown_error');
        } else {
            $smartlinkResponseMessage = $this->config->item('smartlink_response_message');
            $tentative = $_SESSION['tentative'];
            //Ở đây khách đã thanh toán thành công, nên nếu như book vé không thành công thì phải nhận lại thông tin và hỗ trợ gấp cho khách
            if ($checkReturn == 0) {
                $this->load->model('booking_m');
                $result = $this->booking_m->commit();
                if ($result == false) {
                    //Trường hợp này là lỗi kỹ thuật
                    //Mr Chiến chưa cung cấp lỗi cụ thể, mà Soap trả về Fault
                    //Phải gọi lại và hỗ trợ cho khách hàng
                    $this->session->set_flashdata('error', 'Commit Booking Error ' . $tentative->PNR);
                    redirect('flight/unknown_error');
                } else {
                    //Booking thành công, email ticket and show booking number to finish step
                    $this->session->set_userdata('bookingResult', $result->CommitBookingResult);
                    //$this->email($result->CreateBookingResult);
                    redirect('flight/finish');
                }


                //Trường hợp thanh toán không thành công, show lỗi thanh toán từ phía nhà mạng
                //Tiến hành cho khách thanh toán lại
            } else {
                $error = $smartlinkResponseMessage[$checkReturn];
                $this->session->set_flashdata('error', $error);
                $this->template->build('flight/unknown_error', $data);
            }
        }
    }

    /**
     * Email to user when booking success
     * @param type $result
     * @return type
     */
    private function email($result) {
        $data['session'] = $_SESSION;
        $url = 'http://wcf.gotadi.com/EmailContentServices.svc?wsdl';

        //Sending mail to developer and sales
        $client = new SoapClient($url, $this->_soap_options);

        $emailArray = array(
            //Mail developer
            'ToAddress' => 'duy.kieu@gotadi.com',
            'Subject' => 'Khách hàng ' . $_SESSION['passenger']['contact_name'] . ' đặt vé đi ' . $_SESSION['search_data']['acity'],
            'Content' => $this->load->view('flight/email', $data, true),
            'ccAddress' => 'tai.nguyenminh@gotadi.com'
        );

        //Trên môi trường production mới thực hiện gửi mail cho sale và khách hàng
        if (ENVIRONMENT == 'production') {
            $emailArray['ccAddress'] = 'sales@gotadi.com, tai.nguyenminh@gotadi.com, ' . $_SESSION['passenger']['contact_email'];
        }
        return $client->SendMail($emailArray);
    }

    /**
     * Get all Booking Params for MrChien API
     * @return type
     */
    public function showAPI() {
        $this->load->config('api');
        echo $this->config->item('host');
    }

   

    public function test_wsdl() {
        echo '<pre><xmp>';
        print_r($_SESSION);
        echo '</xmp></pre>';
    }


    public function view_email() {
        $this->load->view('email');
    }

    public function about_us() {
        $this->template->build('flight/about_us');
    }

    public function contact() {
        $this->template->build('flight/contact');
    }

    public function policy() {
        $this->template->build('flight/policy');
    }

    public function finish() {
        $this->session->set_userdata('finished', true);
        $this->template->set('search_data', $_SESSION['search_data'])
                ->build('flight/finish');
    }

    public function book() {
        $rules = array(
            array(
                'field' => 'firstname',
                'label' => 'Họ và tên đệm',
                'rules' => 'required|trim'
            ),
            array(
                'field' => 'lastname',
                'label' => 'Tên',
                'rules' => 'required|trim'
            ),
            array(
                'field' => 'email',
                'label' => 'Địa chỉ email',
                'rules' => 'trim|valid_email'
            ),
            array(
                'field' => 'phone',
                'label' => 'Điện thoại',
                'rules' => 'trim|required|numeric'
            ),
            array(
                'field' => 'address',
                'label' => 'Địa chỉ liên hệ',
                'rules' => 'trim|required'
            ),
            array(
                'field' => 'deliver_address',
                'label' => 'Địa chỉ giao vé',
                'rules' => 'trim'
            ),
            array(
                'field' => 'payment_method',
                'label' => 'Phương thức thanh toán',
                'rules' => 'trim|required'
            ),
        );


        if (isset($_GET['selected_brand'])) {
            $brand = $_GET['selected_brand'];
            $this->session->set_userdata(array('selected_brand' => $brand));
        } else if ($this->session->userdata['selected_brand']) {
            $brand = $this->session->userdata('selected_brand');
        } else {
            redirect(base_url());
        }

        if ($brand == 'Vietjet Air') {
            $flights = $this->session->userdata('vietjet_m');
        } else if ($brand == 'Vietnam Airlines') {
            $flights = $this->session->userdata('vna_m');
        } else {
            $flights = $this->session->userdata('jetstar_m');
        }

        if (isset($_GET['selected_class'])) {
            $selected_class = $_GET['selected_class'];
            $this->session->set_userdata(array('selected_class' => $selected_class));
        } else if ($this->session->userdata['selected_class']) {
            $selected_class = $this->session->userdata('selected_class');
        } else {
            redirect(base_url());
        }

        foreach ($flights['outbound'] as $flight) {
            if ($flight['id'] == $selected_class) {
                $data['selected'] = $flight;
            }
        }

        $this->form_validation->set_rules($rules);
        if ($this->form_validation->run()) {
            $this->clients_m->insert_invoice($this->input->post());
        } else {
            $field = new stdClass();
            if ($this->input->post()) {
                foreach ($this->input->post() as $key => $value) {
                    $field->{$key} = $value;
                }
            } else {
                foreach ($rules as $rule) {
                    $field->{$rule['field']} = null;
                }
            }

            $data['field'] = $field;
            $this->load->view('frontend/flight/book', $data);
        }
    }

    public function empty_cart() {
        $this->session->sess_destroy();
        redirect(base_url());
    }

    public function view_session() {
        echo '<pre><xmp>';
        print_r($this->session->userdata());
        echo '</xmp></pre>';
    }

    public function start_search() {
        $this->_all_flights['inbound'] = array();
        $this->_all_flights['outbound'] = array();
        $this->session->set_userdata('result', $this->_all_flights);
    }

    
    public function testVisa()
    {
        $this->load->library('visa_do');
        echo $this->visa_do->setTitle('Test Visa Card')
                ->setAmount('1000000')
                ->setOrderInfo('Chien Test')
                ->setReturnURL(base_url())
                ->getGatewayURL();
    }
    
    public function get_md5($string) {
        echo md5($string);
    }

    public function jetstar($cache = 1) {

        $brand = 'js';

        //Đặt id cho chuyến bay này căn cứ vào search_data và tên hãng
        $id = md5(serialize($_SESSION['search_data']) . 'js');

        if ($this->config->item('cache_enable') == true) {
            //Check cache
            $this->_check_cache('js');
        }


        //Check search data for exist or not
        $this->_check_search_data();

        //Load Jetstar Library
        $this->load->library('jetstar');

        $search_data = $_SESSION['search_data'];

        //Set class parameter
        $this->jetstar->set_dcity_code($search_data['dcity_code']);
        $this->jetstar->set_acity_code($search_data['acity_code']);
        $this->jetstar->set_adult_count($search_data['adult']);
        $this->jetstar->set_child_count($search_data['child']);
        $this->jetstar->set_inf_count($search_data['inf']);
        $this->jetstar->set_ddate($search_data['ddate_submit']);
        $this->jetstar->set_rdate($search_data['rdate_submit']);
        $this->jetstar->set_roundtype($search_data['roundtype']);


        $result = $this->jetstar->run();

        $this->_set_result_session($result);

        //Gửi cho người dùng dữ liệu giá vé
        $data = json_encode($result);
        $this->session->set_userdata($brand, $data);
        //Lưu cache
        if (!empty($result['outbound'])) {
            $this->cache_m->insert($id, $data);
        }
        echo $data;
    }

    public function vietjet() {

        $brand = 'vj';

        //ID for cache
        $id = md5(serialize($_SESSION['search_data']) . $brand);

        if ($this->config->item('cache_enable') == true) {
            $this->_check_cache($brand);
        }

        //Check search data for exist or not
        $this->_check_search_data();

        //Load Vietjet Library
        $this->load->library('vietjet');

        $search_data = $_SESSION['search_data'];

        //Set class parameter
        $this->vietjet->set_dcity_code($search_data['dcity_code']);
        $this->vietjet->set_acity_code($search_data['acity_code']);
        $this->vietjet->set_adult_count($search_data['adult']);
        $this->vietjet->set_child_count($search_data['child']);
        $this->vietjet->set_inf_count($search_data['inf']);
        $this->vietjet->set_ddate($search_data['ddate_submit']);
        $this->vietjet->set_rdate($search_data['rdate_submit']);
        $this->vietjet->set_roundtype($search_data['roundtype']);

        //Run and get Result
        $result = $this->vietjet->run();
        $data = json_encode($result);
        $this->session->set_userdata($brand, $data);
        if (!empty($result['outbound'])) {
            $this->cache_m->insert($id, $data);
        }
        echo $data;
    }

    public function vna() {

        $brand = 'vn';

        $id = md5(serialize($_SESSION['search_data']) . $brand);

        if ($this->config->item('cache_enable') == true) {
            $this->_check_cache($brand);
        }

        //Gọi thư viện Vietnam Airlines
        $this->load->library('vna');

        $search_data = $_SESSION['search_data'];

        //Set class parameter
        $this->vna->set_dcity_code($search_data['dcity_code']);
        $this->vna->set_acity_code($search_data['acity_code']);
        $this->vna->set_adult_count($search_data['adult']);
        $this->vna->set_child_count($search_data['child']);
        $this->vna->set_inf_count($search_data['inf']);
        $this->vna->set_ddate($search_data['ddate_submit']);
        $this->vna->set_rdate($search_data['rdate_submit']);
        $this->vna->set_roundtype($search_data['roundtype']);

        $result = $this->vna->run();
        $data = json_encode($result);
        $this->session->set_userdata($brand, $data);
        //Lưu cache
        if (!empty($result['outbound'])) {
            $this->cache_m->insert($id, $data);
        }

        echo $data;
    }

    public function set_last_session() {
        $this->session->set_userdata('last_search_data', md5(serialize($this->session->userdata['search_data'])));
    }

    public function flight_info($flight_id) {

        $arr = $this->cache_m->cache_array();

        if (count($arr) == 0) {
            redirect();
        }
        foreach ($arr as $item) {
            if ($flight_id == $item->id) {
                $flight = $item;
                break;
            }
        }

        $this->template->set('flight', $flight)
                ->build('flight/info');
    }

    public function contact_info() {
        $this->template->build('flight/contact');
    }

    private function _get_search_param() {
        return $param = 'arrival=' . $this->session->userdata['search_data']['acity_code'] .
                '&departure=' . $this->session->userdata['search_data']['dcity_code'] . ''
                . '&ddate=' . $this->session->userdata['search_data']['ddate_submit'] . ''
                . '&rdate=' . $this->session->userdata['search_data']['rdate_submit'] . '';
    }

    private function _check_cache($brand) {

        $search_id = md5(serialize($_SESSION['search_data']) . $brand);

        $cache = $this->cache_m->get($search_id);

        //Cache
        if ($cache != false) {
            $time = time() - $cache->time;
            if ($time <= $this->config->item('cache_ttl')) {
                $this->session->set_userdata($brand, $cache->data);
                echo $cache->data;
                exit();
            }
        }
    }

    private function _set_result_session($data) {

        function compare($a, $b) {
            $minus = $a->FarePrice->Adult->FareCost - $b->FarePrice->Adult->FareCost;
            return $minus;
        }

        $result = $this->session->userdata['result'];
        $this->session->set_userdata('result', $result);
    }

    public function contact_later() {

        $this->template->build('contact_later');
    }

    private function _check_search_data() {
        if (!isset($_SESSION['search_data']))
            redirect();
    }

    public function select_inbound($id, $brand) {
        $data = array();
        if (isset($_SESSION['vj'])) {
            $vj = json_decode($_SESSION['vj']);
            if (isset($vj->inbound)) {
                $data = array_merge($data, $vj->inbound);
            }
        }
        if (isset($_SESSION['vn'])) {
            $vn = json_decode($_SESSION['vn']);
            if (isset($vn->inbound)) {
                $data = array_merge($data, $vn->inbound);
            }
        }
        if (isset($_SESSION['js'])) {
            $js = json_decode($_SESSION['js']);
            if (isset($js->inbound)) {
                $data = array_merge($data, $js->inbound);
            }
        }

        foreach ($data as $item) {
            if ($item->id == $id) {
                $flight = $item;
                break;
            }
        }
        $this->session->set_userdata('selected_inbound', $flight);
        $this->template->set_layout('empty')
                ->set('class', 'inbound')
                ->set('flight', $flight)
                ->set('way', 'Chiều về:')
                ->build('flight/price_group');
    }

    public function select_outbound($id, $brand) {
        $data = array();
        if (isset($_SESSION['vj'])) {
            $vj = json_decode($_SESSION['vj']);
            if (isset($vj->outbound)) {
                $data = array_merge($data, $vj->outbound);
            }
        }
        if (isset($_SESSION['vn'])) {
            $vn = json_decode($_SESSION['vn']);
            if (isset($vn->outbound)) {
                $data = array_merge($data, $vn->outbound);
            }
        }
        if (isset($_SESSION['js'])) {
            $js = json_decode($_SESSION['js']);
            if (isset($js->outbound)) {
                $data = array_merge($data, $js->outbound);
            }
        }

        foreach ($data as $item) {
            if ($item->id == $id) {
                $flight = $item;
                break;
            }
        }
        $this->session->set_userdata('selected_outbound', $flight);
        $this->template->set_layout('empty')
                ->set('class', 'outbound')
                ->set('flight', $flight)
                ->set('way', 'Chiều đi:')
                ->build('flight/price_group');
    }

    public function update_total_fare() {
        $totalFare = 0;
        if ($_SESSION['search_data']['roundtype'] == 'roundtrip') {
            if (isset($_SESSION['selected_inbound'])) {
                $flight = $_SESSION['selected_inbound'];
                $totalFare += $flight->FarePrice->Total->Total;
            }
        }

        if (isset($_SESSION['selected_outbound'])) {
            $flight = $_SESSION['selected_outbound'];
            $totalFare += $flight->FarePrice->Total->Total;
        }
        $this->session->set_userdata('totalFare', $totalFare);
        echo $totalFare;
    }

    public function set_airlines($airlines) {
        $search_data = $_SESSION['search_data'];
        $search_data['airlines'] = $airlines;
        $this->session->set_userdata('search_data', $search_data);
    }

    public function get_airports() {
        $airports = $this->airports_m->get_all();
        echo json_encode($airports);
    }

    public function check_selected($way = '') {
        if ($way == 'inbound') {
            if (!isset($_SESSION['selected_inbound'])) {
                echo 0;
            } else {
                echo 1;
            }
        } else {
            if (!isset($_SESSION['selected_outbound'])) {
                echo 0;
            } else {
                echo 1;
            }
        }
    }

    

    public function search_data() {
        $response = array('exist' => 1);
        if (!isset($_SESSION['search_data'])) {
            exit(json_encode($response));
        } else {
            $response['dday'] = date('d', $this->get_from_format($_SESSION['search_data']['ddate_submit']));
            $response['dmonth'] = date('m', $this->get_from_format($_SESSION['search_data']['ddate_submit']));
            $response['dyear'] = date('Y', $this->get_from_format($_SESSION['search_data']['ddate_submit']));
            if ($_SESSION['search_data']['roundtype'] == 'oneway') {
                exit(json_encode($response));
            } else {
                $response['rday'] = date('d', $this->get_from_format($_SESSION['search_data']['rdate_submit']));
                $response['rmonth'] = date('m', $this->get_from_format($_SESSION['search_data']['rdate_submit']));
                $response['ryear'] = date('Y', $this->get_from_format($_SESSION['search_data']['rdate_submit']));
                exit(json_encode($response));
            }
        }
    }

    private function get_from_format($timeString) {
        $object = DateTime::createFromFormat('Y-m-d', $timeString);
        return $object->getTimestamp();
    }

    public function noflight() {
        $this->template->set_layout('empty')
                ->build('flight/noflight');
    }

    public function update_airports() {
        $file = file_get_contents('https://raw.githubusercontent.com/jpatokal/openflights/master/data/airports.dat');

        $file = explode("\n", $file);

        array_pop($file);

        $airports = array();

        foreach ($file as $item) {
            $airports[] = explode(',', $item);
        }

        /**
         * 
          Airport ID	Unique OpenFlights identifier for this airport.
          Name	Name of airport. May or may not contain the City name.
          City	Main city served by airport. May be spelled differently from Name.
          Country	Country or territory where airport is located.
          IATA/FAA	3-letter FAA code, for airports located in Country "United States of America".
          3-letter IATA code, for all other airports.
          Blank if not assigned.
          ICAO	4-letter ICAO code.
          Blank if not assigned.
          Latitude	Decimal degrees, usually to six significant digits. Negative is South, positive is North.
          Longitude	Decimal degrees, usually to six significant digits. Negative is West, positive is East.
          Altitude	In feet.
          Timezone	Hours offset from UTC. Fractional hours are expressed as decimals, eg. India is 5.5.
          DST	Daylight savings time. One of E (Europe), A (US/Canada), S (South America), O (Australia), Z (New Zealand), N (None) or U (Unknown). See also: Help: Time
          Tz database time zone	Timezone in "tz" (Olson) format, eg. "America/Los_Angeles".
         */
        foreach ($airports as $item) {
            $ap[] = array(
                'ID' => trim($item[0], '"'),
                'name' => trim($item[1], '"'),
                'city' => trim($item[2], '"'),
                'country' => trim($item[3], '"'),
                'code' => trim($item[4], '"'),
                'icao' => trim($item[5], '"'),
                'latitude' => trim($item[6], '"'),
                'longitude' => trim($item[7], '"'),
                'altitude' => trim($item[8], '"'),
                'timezone' => trim($item[9], '"'),
                'dst' => trim($item[10], '"'),
                'tz' => trim($item[11], '"'),
            );
        }


        foreach ($ap as $item) {
            if ($this->db->where('ID', $item['ID'])->get('airports_full')->num_rows() == 0) {
                $this->db->insert('airports_full', $item);
            }
        }
    }



    /**
     * Hàm này dùng để lấy tổng giá, lấy phí... dùng qua ajax
     * @param type $feeType
     */
    public function update_fare($feeType = 'CASH') {
        $cashFee = 0;
        $atmFee = $this->config->item('atmFee');
        $visaFee = $this->config->item('visaFee');
        $totalFare = $_SESSION['totalFare'];
        $out = $_SESSION['selected_outbound'];
        $outboundFee = $out->FarePrice->Total->Markup;
        $inboundFee = 0;
        if ($_SESSION['search_data']['roundtype'] == 'roundtrip') {
            $in = $_SESSION['selected_inbound'];
            $inboundFee = $in->FarePrice->Total->Markup;
        }

        $serviceFee = $outboundFee + $inboundFee;

        if ($feeType == 'ATM') {
            $totalFare = $atmFee * $totalFare + $totalFare;
            $serviceFee = $outboundFee + $inboundFee + $atmFee * $totalFare;
        } else if ($feeType == 'VISA') {
            $totalFare = $visaFee * $totalFare + $totalFare;
            $serviceFee = $outboundFee + $inboundFee + $visaFee * $totalFare;
        }

        $array = array(
            'totalFare' => number_format(round($totalFare, -3)),
            'serviceFee' => number_format(round($serviceFee, -3)),
        );

        echo json_encode($array);
    }

    /**
     * Hàm này kiểm tra tổng số khách không được vượt quá 12
     * @return boolean
     */
    public function _check_adult() {
        if ($_POST['adult'] + $_POST['child'] > 9) {
            $this->form_validation->set_message('_check_adult', 'Tổng số người lớn và trẻ em không được vượt quá 9');
            return FALSE;
        }

        if ($_POST['adult'] == 0) {
            $this->form_validation->set_message('_check_adult', 'Số người lớn phải lớn hơn 0');
            return FALSE;
        }

        if ($_POST['adult'] + $_POST['child'] + $_POST['inf'] > 12) {
            $this->form_validation->set_message('_check_adult', 'Tổng số khách không được vượt quá 12');
            return FALSE;
        } else {
            return TRUE;
        }
    }

    /**
     * Kiểm tra không cho người dùng nhập tên liên hệ chỉ có một từ
     * @return boolean
     */
    public function _check_contact_name() {
        $contactName = $_POST['contact_name'];
        $numberOfWord = str_word_count($contactName);
        if ($numberOfWord < 2) {
            $this->form_validation->set_message('_check_contact_name', 'Có vẻ bạn đã nhập sai tên liên hệ');
            return FALSE;
        } else {
            return TRUE;
        }
    }

    /**
     * Đọc message để hiểu rõ hơn
     * @return boolean
     */
    public function _check_payment_method() {
        $outbound = $_SESSION['selected_outbound'];
        $departureTime = strtotime($outbound->dtime);
        $currentTime = time();

        $timeHold = $departureTime - $currentTime;
        $hour = ($timeHold - ($timeHold % 3600)) / 3600;
        $minute1 = $timeHold % 3600;
        $minute = ($minute1 - ($minute1 % 60)) / 60;
        $timeHoldString = $hour . ' giờ ' . $minute . ' phút ';
        
        if($outbound->FarePrice->Adult->FareCost == 0 && $_POST['payment_method'] == 'bank_transfer'){
            $this->form_validation->set_message('_check_payment_method', 'Chuyến bay này có giá đặc biệt, bạn phải chọn phương thức thanh toán ngay');
            return FALSE;
        }
        
        
        if ($timeHold < 24 * 3600 && $_POST['payment_method'] == 'bank_transfer') {
            $this->form_validation->set_message('_check_payment_method', 'Chuyến bay này sẽ khởi hành trong khoảng ' . $timeHoldString . ', ít hơn 24 giờ so với quy định, do vậy bạn phải thanh toán ngay. Xin hãy chọn phương thức thanh toán khác');
            return FALSE;
        }
            return TRUE;
        
    }

}
