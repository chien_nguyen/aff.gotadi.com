<table border="0" cellpadding="0" cellspacing="0" height="100%" width="100%" id="bodyTable">
    <tr>
        <td align="center" valign="top">
            <table border="0" cellpadding="20" cellspacing="0" width="700" id="emailContainer">
                <tr>
                    <td align="center" valign="top">
                        <table border="0" cellpadding="20" cellspacing="0" width="100%" id="emailHeader">
                            <tr>
                                <td align="center" valign="top">
                                    <h1 style="text-align:center;">PHIẾU ĐẶT VÉ</h1>
                                    <h3>Loại vé: <?php echo ($_SESSION['search_data']['roundtype'] == 'oneway') ? 'Một chiều' : 'Khứ hồi' ?></h3>
                                    <h4>Mã đặt chỗ: <?php echo $_SESSION['bookingResult']->PNR?></h4>
                                    <hr />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Kính chào quý khách ! <br />
                                    Email này gửi tới quý khách nhằm xác nhận rằng, chúng tôi đã nhận được yêu cầu đặt vé của quý khách. Xin quý khách hãy đọc kỹ các thông tin sau đây,
                                    nếu có thông tin nào không chính xác, xin quý khách gọi ngay đến tổng đài <strong>1900 9002</strong> để được hỗ trợ, cảm ơn quý khách đã sử dụng dịch vụ.
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td align="center" valign="top">
                        <table border="0" cellpadding="20" cellspacing="0" width="100%" id="emailBody">
                            <tr>
                                <td align="center" valign="top">
                                    <h2>THÔNG TIN HÀNH KHÁCH</h2>


                                    <table border="1" cellpadding="5" cellspacing="0" width="100%" id="emailBody">
                                        <tr>
                                            <th>Người lớn/Trẻ em</th>
                                            <th>Họ tên</th>
                                            <th>Ngày sinh</th>
                                        </tr>
                                        <?php for ($i = 1; $i <= $_SESSION['search_data']['adult']; $i++) : ?>
                                            <tr>
                                                <td>Người lớn thứ <?php echo $i ?></td>
                                                <td><?php echo $_SESSION['passenger']['adult_title'][$i] ?> : <?php echo $_SESSION['passenger']['adult_firstname'][$i] . ' ' . $_SESSION['passenger']['adult_lastname'][$i]?> </td>
                                                <td></td>
                                            </tr>
                                        <?php endfor; ?>

                                        <?php for ($i = 1; $i <= $_SESSION['search_data']['child']; $i++) : ?>
                                            <tr>
                                                <td>Trẻ em thứ <?php echo $i ?></td>
                                                <td><?php echo $_SESSION['passenger']['child_title'][$i] ?> : <?php echo $_SESSION['passenger']['child_firstname'][$i] . ' ' . $_SESSION['passenger']['child_lastname'][$i] ?></td>
                                                <td><?php echo $_SESSION['passenger']['child_birthday'][$i] ?></td>
                                            </tr>   
                                        <?php endfor; ?>


                                        <?php for ($i = 1; $i <= $_SESSION['search_data']['inf']; $i++) : ?>
                                            <tr>
                                                <td>Trẻ em thứ <?php echo $i ?></td>
                                                <td><?php echo $_SESSION['passenger']['inf_title'][$i] ?> : <?php echo $_SESSION['passenger']['inf_firstname'][$i] . ' ' .$_SESSION['passenger']['inf_lastname'][$i] ?></td>
                                                <td><?php echo $_SESSION['passenger']['inf_birthday'][$i] ?></td>
                                            </tr>
                                        <?php endfor; ?>


                                    </table>
                                    <h2>THÔNG TIN LIÊN HỆ</h2>
                                    <table border="1" cellpadding="5" cellspacing="0" width="100%" id="emailBody">
                                        <tr>
                                            <th>Họ và tên</th>
                                            <th>Số thứ nhất:</th>
                                            <th>Số thứ hai:</th>
                                            <th>Email</th>
                                            <th>Địa chỉ</th>
                                        </tr>
                                        <tr>
                                            <td><?php echo $_SESSION['passenger']['contact_title'] ?> : <?php echo $_SESSION['passenger']['contact_name'] ?></td>
                                            <td><?php echo $_SESSION['passenger']['contact_phone'] ?></td>
                                            <td><?php echo $_SESSION['passenger']['contact_phone_2'] ?></td>
                                            <td><?php echo $_SESSION['passenger']['contact_email'] ?></td>
                                            <td><?php echo $_SESSION['passenger']['contact_address'] ?></td>
                                        </tr>
                                    </table>


                                    <?php $outbound = $_SESSION['selected_outbound']; ?>
                                    <h2>CHUYẾN BAY CHIỀU ĐI</h2>
                                    <table border="1" cellpadding="5" cellspacing="0" width="100%" id="emailBody">
                                        <tr>
                                            <td width="50%" align="right">Airlines </td>
                                            <td width="50%" align="left"><strong><?php echo $outbound->Brand ?></strong></td>
                                        </tr>
                                        <tr>
                                            <td width="50%" align="right">Flight Number</td>
                                            <td width="50%" align="left"><strong><?php echo $outbound->FlightNumber ?></strong></td>
                                        </tr>
                                        <tr>
                                            <td width="50%" align="right">Departure Station</td>
                                            <td width="50%" align="left"><strong><?php echo $outbound->DepartureStation ?></strong></td>
                                        </tr>
                                        <tr>
                                            <td width="50%" align="right">Arrival Station</td>
                                            <td width="50%" align="left"><strong><?php echo $outbound->ArrivalStation ?></strong></td>
                                        </tr>
                                        <tr>
                                            <td width="50%" align="right">Departure City</td>
                                            <td width="50%" align="left"><strong><?php echo $outbound->dcity ?></strong></td>
                                        </tr>
                                        <tr>
                                            <td width="50%" align="right">Arrival City</td>
                                            <td width="50%" align="left"><strong><?php echo $outbound->acity ?></strong></td>
                                        </tr>
                                        <tr>
                                            <td width="50%" align="right">Departure Date</td>
                                            <td width="50%" align="left"><strong><?php echo $outbound->DDATE ?></strong></td>
                                        </tr>
                                        <tr>
                                            <td width="50%" align="right">Arrival Date</td>
                                            <td width="50%" align="left"><strong><?php echo $outbound->ADATE ?></strong></td>
                                        </tr>
                                        <tr>
                                            <td width="50%" align="right">STD</td>
                                            <td width="50%" align="left"><strong><?php echo $outbound->STD ?></strong></td>
                                        </tr>
                                        <tr>
                                            <td width="50%" align="right">STA</td>
                                            <td width="50%" align="left"><strong><?php echo $outbound->STA ?></strong></td>
                                        </tr>
                                        <tr>
                                            <td width="50%" align="right">Class of Service</td>
                                            <td width="50%" align="left"><strong><?php echo $outbound->ClassOfService ?></strong></td>
                                        </tr>
                                        <tr>
                                            <td width="50%" align="right">Fare Cost</td>
                                            <td width="50%" align="left"><strong><?php echo number_format($outbound->FarePrice->Total->FareCost) ?></strong></td>
                                        </tr>
                                        <tr>
                                            <td width="50%" align="right">Tax</td>
                                            <td width="50%" align="left"><strong><?php echo number_format($outbound->FarePrice->Total->Tax) ?></strong></td>
                                        </tr>
                                        <tr style="font-size: 22px;">
                                            <td width="50%" align="right">Total</td>
                                            <td width="50%" align="left"><strong><?php echo number_format($outbound->FarePrice->Total->Total) ?></strong></td>
                                        </tr>
                                    </table>

                                    <?php if ($_SESSION['search_data']['roundtype'] == 'roundtrip') : ?>
                                        <h2>CHUYẾN BAY CHIỀU VỀ</h2>

                                        <?php $inbound = $_SESSION['selected_inbound'] ?>
                                        <table border="1" cellpadding="5" cellspacing="0" width="100%" id="emailBody">
                                            <tr>
                                                <td width="50%" align="right">Airlines </td>
                                                <td width="50%" align="left"><strong><?php echo $inbound->Brand ?></strong></td>
                                            </tr>
                                            <tr>
                                                <td width="50%" align="right">Flight Number</td>
                                                <td width="50%" align="left"><strong><?php echo $inbound->FlightNumber ?></strong></td>
                                            </tr>
                                            <tr>
                                                <td width="50%" align="right">Departure Station</td>
                                                <td width="50%" align="left"><strong><?php echo $inbound->DepartureStation ?></strong></td>
                                            </tr>
                                            <tr>
                                                <td width="50%" align="right">Arrival Station</td>
                                                <td width="50%" align="left"><strong><?php echo $inbound->ArrivalStation ?></strong></td>
                                            </tr>
                                            <tr>
                                                <td width="50%" align="right">Departure City</td>
                                                <td width="50%" align="left"><strong><?php echo $inbound->dcity ?></strong></td>
                                            </tr>
                                            <tr>
                                                <td width="50%" align="right">Arrival City</td>
                                                <td width="50%" align="left"><strong><?php echo $inbound->acity ?></strong></td>
                                            </tr>
                                            <tr>
                                                <td width="50%" align="right">Departure Date</td>
                                                <td width="50%" align="left"><strong><?php echo $inbound->DDATE ?></strong></td>
                                            </tr>
                                            <tr>
                                                <td width="50%" align="right">Arrival Date</td>
                                                <td width="50%" align="left"><strong><?php echo $inbound->ADATE ?></strong></td>
                                            </tr>
                                            <tr>
                                                <td width="50%" align="right">STD</td>
                                                <td width="50%" align="left"><strong><?php echo $inbound->STD ?></strong></td>
                                            </tr>
                                            <tr>
                                                <td width="50%" align="right">STA</td>
                                                <td width="50%" align="left"><strong><?php echo $inbound->STA ?></strong></td>
                                            </tr>
                                            <tr>
                                                <td width="50%" align="right">Class of Service</td>
                                                <td width="50%" align="left"><strong><?php echo $inbound->ClassOfService ?></strong></td>
                                            </tr>
                                            <tr>
                                                <td width="50%" align="right">Fare Cost</td>
                                                <td width="50%" align="left"><strong><?php echo number_format($inbound->FarePrice->Total->FareCost) ?></strong></td>
                                            </tr>
                                            <tr>
                                                <td width="50%" align="right">Tax</td>
                                                <td width="50%" align="left"><strong><?php echo number_format($inbound->FarePrice->Total->Tax) ?></strong></td>
                                            </tr>
                                            <tr style="font-size: 22px;">
                                                <td width="50%" align="right">Total</td>
                                                <td width="50%" align="left"><strong><?php echo number_format($inbound->FarePrice->Total->Total) ?></strong></td>
                                            </tr>
                                        </table>

                                    <?php endif; ?>

                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td align="center" valign="top">
                        <table border="0" cellpadding="20" cellspacing="0" width="100%" id="emailFooter">
                            <tr>
                                <td align="center" valign="top">
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>