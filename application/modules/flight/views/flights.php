<ul class="return-outbound">
    <?php foreach ($flights['outbound'] as $item) : ?>

        <?php
        if ($item->B == 'js') {
            $classs = 'Eco-' . $item->ClassOfService;
        } else if ($item->B == 'vj') {
            if ($item->ClassOfService == 'Eco') {
                $classs = 'Economy';
            }
        } else {
            $classs = $item->ClassOfService;
        }
        ?>
    <li data-brand="<?php echo $item->B?>" id="<?php echo $item->id?>" class="item">
            <div class="col-xs-3 b">
                <span class="duration"><img src="<?php echo $item->IconName?>"></span>
                <div class="class"><?php echo $classs ?></div>
            </div>
            <div class="col-xs-5 b">
                <span class="time"><?php echo $item->STD . ' - ' . $item->STA ?></span>
                <div class="small">Chuyến bay thẳng</div>
            </div>
            <div class="col-xs-4 b">
                <span class="price"><?php echo number_format($item->FarePrice->Total->Total) ?> <span class="small">đ</span></span>
                <div class="small">Đã gồm thuế</div>
            </div>
            <div class="col-xs-12 flight-detail">
                <ul class="list-unstyled">
                    <li>Hãng: <span class="pull-right">Vietjet Air</span></li>
                    <li>Chuyến bay: <span class="pull-right">VJ168</span></li>
                    <li>Từ: <span class="pull-right">13:00 - Hồ Chí Minh</span></li>
                    <li>Đến: <span class="pull-right">15:05 - Hà Nội</span></li>
                    <li>Ngày bay: <span class="pull-right">03-10-2015</span></li>
                    <li>Giá vé: <span class="pull-right">699,000</span></li>
                    <li>Phí và thuế: <span class="pull-right">289,900</span></li>
                    <li class="price">Tổng cộng: <span class="pull-right">988,900</span></li>
                </ul>
                <div class="button-select-flight"><a class="pull-left close-item" href="#"><i class="fa fa-close"></i> Đóng</a>  <button class="btn btn-success pull-right">Chọn chuyến bay này</button></div>
            </div>
        </li>

<?php endforeach; ?>
</ul>

