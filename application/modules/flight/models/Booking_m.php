<?php

defined('BASEPATH') or exit('Không được quyền truy cập');

class Booking_m extends CI_Model {

    private $_url;
    private $_client;
    private $_soap_options = array(
        'soap_version' => SOAP_1_1,
        'exceptions' => true,
        'encoding' => 'UTF-8',
        'content-type' => 'text/xml'
    );

    public function __construct() {
        parent::__construct();

        $this->_url = $this->config->item('bookingURL');
        try{
            $this->_client = new SoapClient($this->_url, $this->_soap_options);
        } catch (SoapFault $ex) {
            return false;
        }
    }

    /**
     * Tạo booking
     * @param type $bookingParams
     * @return boolean
     */
    public function create() {
        $bookingParams = $this->get_booking_params();
        try {
            $result = $this->_client->CreateBooking($bookingParams);
            if ($result) {
                return $result;
            } else {
                return false;
            }
        } catch (Exception $ex) {
            return false;
        }
    }

    /**
     * 
     * @return boolean
     */
    public function commit() {
        $tentative = $_SESSION['tentative'];
        $params = $this->get_booking_params('COMMIT', $tentative->PNR, $tentative->PNR, $tentative->ID, $tentative);
        try {
            $result = $this->_client->CommitBooking($params);
            if ($result) {
                return $result;
            } else {
                return false;
            }
        } catch (Exception $ex) {
            return false;
        }
    }

    /**
     * 
     * @return boolean
     */
    public function create_tentative() {
        $params = $this->get_booking_params('TENTATIVE');
        try {
            $result = $this->_client->CreateBooking($params);
            if ($result) {
                return $result;
            } else {
                return false;
            }
        } catch (Exception $ex) {
            return false;
        }
    }

    /**
     * 
     * @param type $bookingType
     * @param type $tentativeID
     * @param type $paymentRef
     * @param type $refferenceID
     * @param type $tentative
     * @return type
     */
    private function get_booking_params($bookingType = 'HOLD', $tentativeID = false, $paymentRef = false, $refferenceID = false, $tentative = false) {

        if (isset($_SESSION['selected_inbound'])) {
            $in = $_SESSION['selected_inbound'];
        }
        $out = $_SESSION['selected_outbound'];
        $pax = $_SESSION['passenger'];
        $query = $_SESSION['search_data'];

        $bookingParams = array(
            //Loại booking 'HOLD', 'TENTATIVE', 'COMMIT'
            'ReservationType' => $bookingType,
            'BookingDetail' => array(
                //Lấy affiliate trong SESSION
                'AffiliateID' => (isset($_SESSION['aff'])) ? $_SESSION['aff'] : '',
                'AirProviderSession' => array(
                    'AirProvider' => str_replace(' ', '', $out->Brand),
                ),
                'ArrivalLocation' => $query['acity_code'],
                'BookingID' => '',
                'ContactDetail' => array(
                    'Email' => $pax['contact_email'],
                    'FirstName' => strtoupper(removesign($this->get_first_name($pax['contact_name']))),
                    'LastName' => strtoupper(removesign($this->get_last_name($pax['contact_name']))),
                    'Phone1' => $pax['contact_phone'],
                    'Phone2' => $pax['contact_phone_2'],
                    'Title' => $this->get_title($pax['contact_title']),
                    'City' => 'HCM'
                ),
                'CreatedDate' => date('Y-m-d', time()) . 'T00:00:00.000+' . date('H:i', time()),
                'DepartureLocation' => $query['dcity_code'],
                'Itineraries' => array(
                    'ItineraryDetailDO' => array(
                        array(
                            'AirSupplier' => ($out->B == 'js') ? 'BL' : strtoupper($out->B),
                            'ArrivalLocationCode' => $out->ArrivalStation,
                            'ArrivalTime' => $out->atime,
                            'ClassCode' => $out->ClassOfService,
                            'FareCode' => $out->FareCode,
                            'CarrierCode' => ($out->B == 'js') ? 'BL' : ($out->B == 'vj') ? 'VJ' : 'VN',
                            'DepartureLocationCode' => $out->DepartureStation,
                            'DepartureTime' => $out->dtime,
                            'FareBasic' => $out->FarePrice->Total->FareCost,
                            'FlightNo' => $out->FlightNumber,
                            'Markup' => $out->FarePrice->Total->Markup,
                            'ReturnTime' => '',
                            'RouteType' => 'DEPARTURE',
                            'Tax' => $out->FarePrice->Total->Tax,
                        ),
                    //Xem thêm chặng về
                    )
                ),
                'Passengers' => array(
                ),
                'RouteType' => strtoupper($query['roundtype']),
                'PaymentMode' => ($pax['payment_method'] == 'internet_banking') ? 'Wallet' : (($pax['payment_method'] == 'visa_master') ? 'CreditCard' : 'Cash'),
                'TotalAdult' => $query['adult'],
                'TotalChild' => $query['child'],
                'TotalInfant' => $query['inf'],
                'TotalAmount' => round($_SESSION['totalFare'] + $this->get_payment_fee(), -3),
                'UpdateDate' => date('Y-m-d', time()) . 'T00:00:00.000+' . date('H:i', time())
            )
        );

        if ($tentative != false) {
            $bookingParams['TentativeID'] = $tentative->PNR;
        }

        if ($paymentRef == false) {
            $bookingParams['BookingDetail']['PaymentRef'] = $_SESSION['paymentRef'];
        } else {
            $bookingParams['BookingDetail']['PaymentRef'] = $tentative->PNR;
        }


        if ($paymentRef == false) {
            $bookingParams['RefferenceID'] = '';
        } else {
            $bookingParams['RefferenceID'] = $tentative->ID;
        }

        if ($out->B == 'js') {
            $bookingParams['BookingDetail']['AirProviderSession']['JetstarFareKeyDepart'] = $out->FareKey;
            $bookingParams['BookingDetail']['AirProviderSession']['JetstarJourneyKeyDepart'] = $out->JourneyKey;
            $bookingParams['BookingDetail']['AirProviderSession']['TokenKey'] = 'NN';
            $bookingParams['BookingDetail']['AirSupplier'] = 'BL';
        }
        if ($out->B == 'vj') {
            $bookingParams['BookingDetail']['AirProviderSession']['TokenKey'] = '4440274f-8482-43d5-919f-5e012d561603';
        }

        if ($query['roundtype'] == 'roundtrip') {
            if ($in->B == 'js') {
                $bookingParams['BookingDetail']['AirProviderSession']['JetstarFareKeyReturn'] = $in->FareKey;
                $bookingParams['BookingDetail']['AirProviderSession']['JetstarJourneyKeyReturn'] = $in->JourneyKey;
            }
        }


        $passenger = array();

        for ($i = 1; $i <= $query['adult']; $i++) :
            $passenger[] = array(
                'Email' => $pax['contact_email'],
                'FirstName' => strtoupper(removesign($pax['adult_firstname'][$i])),
                'LastName' => strtoupper(removesign($pax['adult_lastname'][$i])),
                'Phone1' => '',
                'Phone2' => '',
                'Title' => $this->get_title($pax['adult_title'][$i]),
                'PassengerType' => 'ADT'
            );
        endfor;

        for ($i = 1; $i <= $query['child']; $i++) :
            $passenger[] = array(
                'Email' => '',
                'FirstName' => strtoupper(removesign($pax['child_firstname'][$i])),
                'LastName' => strtoupper(removesign($pax['child_lastname'][$i])),
                'Phone1' => '',
                'Phone2' => '',
                'Title' => $this->get_title_child($pax['child_title'][$i]),
                'PassengerType' => 'CHD'
            );
        endfor;


        for ($i = 1; $i <= $query['inf']; $i++) :
            $passenger[] = array(
                'Email' => '',
                'FirstName' => strtoupper(removesign($pax['inf_firstname'][$i])),
                'LastName' => strtoupper(removesign($pax['inf_lastname'][$i])),
                'Phone1' => '',
                'Phone2' => '',
                'Title' => $this->get_title_child($pax['inf_title'][$i]),
                'PassengerType' => 'INF'
            );
        endfor;


        if ($query['adult'] + $query['child'] + $query['inf'] == 1) {
            $bookingParams['BookingDetail']['Passengers']['PassengerDetailDO'] = $passenger[0];
        } else {
            $bookingParams['BookingDetail']['Passengers']['PassengerDetailDO'] = $passenger;
        }



        //Chiều về
        if ($query['roundtype'] == 'roundtrip') {
            $bookingParams['BookingDetail']['Itineraries']['ItineraryDetailDO'][1] = array(
                'AirSupplier' => ($in->B == 'js') ? 'BL' : strtoupper($in->B),
                'ArrivalLocationCode' => $in->ArrivalStation,
                'ArrivalTime' => $in->atime,
                'ClassCode' => $in->ClassOfService,
                'FareCode' => $in->FareCode,
                'CarrierCode' => ($out->B == 'js') ? 'BL' : ($out->B == 'vj') ? 'VJ' : 'VN',
                'DepartureLocationCode' => $in->DepartureStation,
                'DepartureTime' => $in->dtime,
                'FareBasic' => $in->FarePrice->Total->FareCost,
                'FlightNo' => $in->FlightNumber,
                'Markup' => $in->FarePrice->Total->Markup,
                'ReturnTime' => '',
                'RouteType' => 'RETURN',
                'Tax' => $in->FarePrice->Total->Tax,
            );
        }
        return $bookingParams;
    }

    /**
     * 
     * @param type $name
     * @return type
     */
    private function get_first_name($name) {
        $array = explode(' ', $name);
        $n = count($array);
        unset($array[$n - 1]);
        $firstName = '';
        foreach ($array as $word) {
            $firstName .= $word . ' ';
        }

        return trim($firstName);
    }

    /**
     * 
     * @param type $name
     * @return type
     */
    private function get_last_name($name) {
        $array = explode(' ', $name);
        $n = count($array);
        if (count($array) == 1) {
            $lastName = $array[0];
        } else {
            $lastName = $array[$n - 1];
        }
        return $lastName;
    }

    /**
     * 
     * @param type $title
     * @return type
     */
    private function get_title($title) {
        return ($title == 'Ông') ? 'MR' : 'MS';
    }

    /**
     * 
     * @param type $title
     * @return type
     */
    private function get_title_child($title) {
        return ($title == 'Bé trai') ? 'MSTR' : 'MISS';
    }

    /**
     * Lấy phí thanh toán
     * @return type
     */
    private function get_payment_fee() {
        $pax = $_SESSION['passenger'];
        $atmFee = $this->config->item('atmFee');
        $visaFee = $this->config->item('visaFee');


        if ($pax['payment_method'] == 'internet_banking') {
            $fee = $_SESSION['totalFare'] * $atmFee;
        } else if ($pax['payment_method'] == 'visa_master') {
            $fee = $_SESSION['totalFare'] * $visaFee;
        } else {
            $fee = 0;
        }

        return $fee;
    }

}
