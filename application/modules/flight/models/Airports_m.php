<?php
/**
 * Created by PhpStorm.
 * User: duy_t
 * Date: 6/24/2015
 * Time: 1:30 PM
 */

class Airports_m extends CI_Model{

    public function __construct()
    {
        parent::__construct();
    }

    
    public function get_all()
    {
        return $this->db->get('airports')->result();
    }
    
    public function get_info($code)
    {
        $query = $this->db->where('code', $code)->get('airports');
        
        if($query->num_rows() > 0)
        {
            return $query->row();
        }
        return false;

    }
    
    public function get_city($airport_code)
    {
        $query = $this->db->where('code', $airport_code)->get('airports');
        if($query->num_rows() == 1)
        {
            return $query->row()->city;
        }
    }
}