<?php

defined('BASEPATH') or exit('Không được quyền truy cập');

class Cache_m extends CI_Model{
    public function __construct() {
        parent::__construct();
    }
    
    public function get($id)
    {
        $query = $this->db->where('id', $id)->get('cache');
        if($query->num_rows() == 1)
        {
            return $query->row();
        }
        return false;
    }
    
    
    public function insert($id, $data)
    {
        $query = $this->db->where('id', $id)->get('cache');
        if($query->num_rows() == 1)
        {
            $cache = array(
                'data' => $data,
                'time'  => time()
            );
            $this->db->where('id', $id)->update('cache', $cache);
        }else{
            $cache = array(
                'id'    => $id,
                'data' => $data,
                'time'  => time()
            );
            $this->db->insert('cache', $cache);
        }
        
        if($this->db->affected_rows() > 0)
        {
            return true;
        }else{
            return false;
        }
    }
    
    //Chỉ được gọi hàm này sau khi người dùng đã tiền hành search vé
    public function cache_array()
    {
        $session = $this->session->userdata['search_data'];
        
        if(!isset($session)){
            redirect();
        }
        
        //Tồn tại session
        $vn_id = md5(serialize($session ). 'vna');
        $vj_id = md5(serialize($session ). 'vietjet');
        $js_id = md5(serialize($session ). 'jetstar');
        
        $vn_query = $this->db->where('id', $vn_id)->get('cache');
        $js_query = $this->db->where('id', $js_id)->get('cache');
        $vj_query = $this->db->where('id', $vj_id)->get('cache');
        
        $arr = array();
        if($vn_query->num_rows() == 1){
            $row = $vn_query->row();
            $tmp = json_decode($row->data);
            if(isset($tmp->inbound)){
                $arr = array_merge($arr, $tmp->inbound);
            }
            if(isset($tmp->outbound)){
                $arr = array_merge($arr, $tmp->outbound);
            }
        }
        if($js_query->num_rows() == 1){
            $row = $js_query->row();
            $tmp = json_decode($row->data);
            if(isset($tmp->inbound)){
                $arr = array_merge($arr, $tmp->inbound);
            }
            if(isset($tmp->outbound)){
                $arr = array_merge($arr, $tmp->outbound);
            }
        }
        if($vj_query->num_rows() == 1){
            $row = $vj_query->row();
            $tmp = json_decode($row->data);
            if(isset($tmp->inbound)){
                $arr = array_merge($arr, $tmp->inbound);
            }
            if(isset($tmp->outbound)){
                $arr = array_merge($arr, $tmp->outbound);
            }
        }
        
        return $arr;
    }
}
